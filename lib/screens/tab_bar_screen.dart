import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/screens/course/course_model.dart';
import 'package:tense/screens/course/course_screen.dart';
import 'package:tense/screens/exercises/exercises_model.dart';
import 'package:tense/screens/exercises/exercises_screen.dart';
import 'package:tense/screens/profile/profile_screen.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

import 'onboarding/onboarding_model.dart';
import 'onboarding/onboarding_screen.dart';

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    CourseScreen(
      model: CourseModel(),
    ),
    ExercisesScreen(
      model: ExercisesModel(),
    ),
    ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> _getOnboardingPref() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    return sharedPref.getBool(SharedPrefsKeys.onboardingDidShow);
  }

  @override
  void initState() {
    super.initState();
    _getOnboardingPref().then((value) {
      if (value == null) {
        Timer(Duration(milliseconds: 350), () {
          final route = MaterialPageRoute(builder: (BuildContext context) {
            return OnboardingScreen(
              model: OnboardingModel(),
            );
          });
          Navigator.of(context).push(route);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      backgroundColor: Colors.white,
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.payment),
              title: Text(
                'Курс',
                style: TextStyle(fontWeight: FontWeight.w500),
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.list),
              title: Text(
                'Задания',
                style: TextStyle(fontWeight: FontWeight.w500),
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              title: Text(
                'Профиль',
                style: TextStyle(fontWeight: FontWeight.w500),
              )),
        ],
        backgroundColor: Colors.grey[100],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent,
        onTap: _onItemTapped,
      ),
    );
  }
}
