import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:page_view_indicators/page_view_indicators.dart';
import 'package:tense/custom_widgets/subscriptions/price_button.dart';
import 'package:tense/custom_widgets/subscriptions/price_calc_text.dart';
import 'package:tense/custom_widgets/top_bar.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/subscriptions/subscription_model.dart';

class SubscriptionsScreen extends StatefulWidget {
  final SubscriptionsModel model;

  const SubscriptionsScreen({Key key, this.model}) : super(key: key);
  @override
  _SubscriptionsScreenState createState() => _SubscriptionsScreenState();
}

class _SubscriptionsScreenState extends State<SubscriptionsScreen> {
  List<Widget> slides = [];
  final _selectedPage = ValueNotifier<int>(0);
  final _pageController = PageController(
    initialPage: 0,
  );

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  Widget _providePurchaseButtons() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        PriceButton(
          handler: () {},
          text: 'Цена 1',
        ),
        SizedBox(height: 15),
        PriceButton(
          handler: () {},
          text: 'Цена 2',
        ),
        PriceCalcText(text: 'Всего ___ в месяц...'),
        PriceButton(
          handler: () {},
          text: 'Цена 3',
        ),
        PriceCalcText(text: 'Всего ___ в месяц...'),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey[350],
              offset: Offset(0, 3),
              blurRadius: 12.0,
            ),
          ],
        ),
        child: Flex(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          direction: Axis.vertical,
          children: <Widget>[
            TopBar(
              hasTopSafeAreaMargin: false,
              handler: () {
                Navigator.of(context).pop();
              },
              headerText: 'Tense+',
            ),
            Expanded(
              flex: 2,
              child: CarouselSlider(
                items: null,
                options: CarouselOptions(
                  enableInfiniteScroll: false,
                  height: MediaQuery.of(context).size.height * 0.8,
                  viewportFraction: 1.0,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _selectedPage.value = index;
                    });
                  },
                ),
              ),
            ),
            CirclePageIndicator(
              currentPageNotifier: _selectedPage,
              itemCount: widget.model.numberOfPages,
              selectedDotColor: TensesData.groupColors[5],
              dotColor: Colors.grey[400],
              size: 10,
              selectedSize: 10,
              dotSpacing: 8,
            ),
            SizedBox(
              height: 20,
            ),
            _providePurchaseButtons(),
            CupertinoButton(
              child: Text(
                'Восстановить',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'AvenirNext',
                  fontWeight: FontWeight.w600,
                  color: Colors.grey[800],
                ),
              ),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
