import 'package:tense/data/subscriptions_data.dart';

class SubscriptionsModel {
  int get numberOfPages {
    return SubscriptionsData.headers.length;
  }
}
