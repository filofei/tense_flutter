import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tense/custom_widgets/exercises/custom_ios_list_view.dart';
import 'package:tense/custom_widgets/exercises/exercises_header.dart';
import 'package:tense/custom_widgets/exercises/exercises_tile.dart';
import 'package:tense/custom_widgets/tab_bar_top_bar.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/exercises/exercises_model.dart';
import 'package:tense/screens/subscriptions/subscription_model.dart';
import 'package:tense/screens/subscriptions/subscriptions_screen.dart';

class ExercisesScreen extends StatefulWidget with FirstLaunchMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final ExercisesModel model;

  ExercisesScreen({this.model});

  @override
  _ExercisesScreenState createState() => _ExercisesScreenState();
}

class _ExercisesScreenState extends State<ExercisesScreen> {
  double _sectionHeaderHeight(BuildContext context, int section) {
    return 30.0;
  }

  double _cellHeight(BuildContext context, int section, int row) {
    return 60.0;
  }

  Widget _sectionHeaderBuilder(BuildContext context, int section) {
    return ExercisesHeader(
      title: widget.model.sectionTitle(section),
      color: widget.model.sectionColor(section),
    );
  }

  Widget _cellBuilder(BuildContext context, int section, int row) {
    return ExercisesTile(
      model: widget.model.tileModel(row, section),
    );
  }

  @override
  Widget build(BuildContext context) {
    widget.showMessageIfFirstLaunch(widget, Messages.exercisesVC, context);
    return Container(
      child: Column(
        children: <Widget>[
          TabBarTopBar(
              handler: () {
                MaterialPageRoute route =
                    MaterialPageRoute(builder: (BuildContext context) {
                  return SubscriptionsScreen(
                    model: SubscriptionsModel(),
                  );
                });
                Navigator.of(context).push(route);
              },
              headerText: widget.model.title()),
          Expanded(
            flex: 2,
            child: FlutterTableView(
                sectionCount: widget.model.sectionCount,
                rowCountAtSection: widget.model.rowCountAtSection,
                sectionHeaderBuilder: _sectionHeaderBuilder,
                cellBuilder: _cellBuilder,
                sectionHeaderHeight: _sectionHeaderHeight,
                cellHeight: _cellHeight),
          )
        ],
      ),
    );
  }
}
