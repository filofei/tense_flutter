import 'dart:ui';

import 'package:tense/data/tenses_data.dart';
import 'exercises_tile_model.dart';

class ExercisesModel {
  String title() {
    return 'Выберите задание';
  }

  int sectionCount = TensesData.groupNames.length - 1;

  int rowCountAtSection(int section) {
    return (section == 0)
        ? TensesData.firstExercisesGroup[section].item1.length
        : TensesData.tenses[section + 1].item1.length;
  }

  Color sectionColor(int section) {
    return TensesData.tenses[section + 1].item2;
  }

  String sectionTitle(int section) {
    return (section == 0)
        ? "Simple + to be"
        : TensesData.groupNames[section + 1];
  }

  ExercisesTileModel tileModel(int index, int section) {
    final tileModel = ExercisesTileModel(
        index: index,
        title: (section == 0)
            ? TensesData.firstExercisesGroup[section].item1[index]
            : TensesData.tenses[section + 1].item1[index],
        color: (section == 0)
            ? TensesData.groupColors[1]
            : TensesData.tenses[section + 1].item2);
    return tileModel;
  }
}
