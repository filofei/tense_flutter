import 'dart:ui';

class ExercisesTileModel {
  final int index;
  final String title;
  final Color color;

  const ExercisesTileModel({
    this.index,
    this.title,
    this.color,
  });
}
