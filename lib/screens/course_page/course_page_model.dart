class CoursePageModel {
  String tenseName;

  CoursePageModel({this.tenseName});

  int numberOfPages() {
    return 3;
  }

  String title() {
    return tenseName;
  }

  String backTitle() {
    return 'Назад';
  }

  String forwardTitle() {
    return 'Далее';
  }
}
