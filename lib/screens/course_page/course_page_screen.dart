import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:page_view_indicators/circle_page_indicator.dart';
import 'package:swipe_gesture_recognizer/swipe_gesture_recognizer.dart';
import 'package:tense/custom_widgets/top_bar.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/course_test/course_test_model.dart';
import 'package:tense/screens/course_test/course_test_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'course_page_model.dart';

class CoursePageScreen extends StatefulWidget with FirstLaunchMixin {
  final CoursePageModel model;

  CoursePageScreen({this.model});

  @override
  _CoursePageScreenState createState() => _CoursePageScreenState();
}

class _CoursePageScreenState extends State<CoursePageScreen> {
  final _currentPageNotifier = ValueNotifier<int>(0);

  _goBack() {
    if (_currentPageNotifier.value == 0) {
      Navigator.of(context).pop();
    }
    setState(() {
      _currentPageNotifier.value = _currentPageNotifier.value - 1;
    });
  }

  _moveOn() {
    if (_currentPageNotifier.value == widget.model.numberOfPages() - 1) {
      _openTestScreen();
      return;
    }
    setState(() {
      _currentPageNotifier.value = _currentPageNotifier.value + 1;
    });
  }

  _openTestScreen() {
    final testRoute = CourseTestScreen(
      model: CourseTestModel(widget.model.tenseName),
    );
    Navigator.push(context, MaterialPageRoute(builder: (context) => testRoute));
  }

  @override
  Widget build(BuildContext context) {
    widget.showMessageIfFirstLaunch(widget, Messages.coursePageVC, context);
    return Scaffold(
      body: Column(
        children: [
          TopBar(
            headerText: widget.model.title(),
            handler: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            child: SwipeGestureRecognizer(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[350],
                      blurRadius: 9,
                      offset: Offset(0, 3),
                    ),
                  ],
                  color: Colors.white,
                ),
                margin: const EdgeInsets.fromLTRB(20, 5, 20, 20),
                padding: const EdgeInsets.all(4.0),
                child: WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                ),
              ),
              onSwipeRight: () {
                _goBack();
              },
              onSwipeLeft: () {
                _moveOn();
              },
            ),
          ),
          CirclePageIndicator(
            itemCount: widget.model.numberOfPages(),
            selectedDotColor: TensesData.groupColors[5],
            dotColor: Colors.grey[350],
            dotSpacing: 5,
            currentPageNotifier: _currentPageNotifier,
          ),
          Container(
            height: 44,
            margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border:
                        Border.all(width: 3, color: TensesData.groupColors[5]),
                  ),
                  child: CupertinoButton(
                    borderRadius: BorderRadius.circular(22),
                    padding: EdgeInsets.symmetric(horizontal: 35),
                    color: Colors.transparent,
                    child: Container(
                      child: Text(
                        widget.model.backTitle(),
                        style: TextStyle(
                            fontFamily: 'AvenirNext',
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: TensesData.groupColors[5]),
                      ),
                    ),
                    onPressed: _goBack,
                  ),
                ),
                CupertinoButton(
                  borderRadius: BorderRadius.circular(22),
                  padding: EdgeInsets.symmetric(horizontal: 35),
                  color: TensesData.groupColors[5],
                  child: Text(
                    widget.model.forwardTitle(),
                    style: TextStyle(
                        fontFamily: 'AvenirNext',
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  onPressed: _moveOn,
                )
              ],
            ),
          )
        ],
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}
