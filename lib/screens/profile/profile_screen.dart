import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/custom_widgets/profile/progress_circle.dart';
import 'package:tense/custom_widgets/textfield_alert.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/settings/settings_model.dart';
import 'package:tense/screens/settings/settings_screen.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

class ProfileScreen extends StatefulWidget with FirstLaunchMixin {
  final double imageSide = 100;
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File _image;
  bool _imageVisible = false;
  String _name;
  final picker = ImagePicker();

  Future pickImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      prefs.setString(SharedPrefsKeys.imagePath, pickedFile.path);
    }
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  _retrieveImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final path = prefs.getString(SharedPrefsKeys.imagePath);
    setState(() {
      if (path != null) {
        _image = File(path).existsSync() ? File(path) : null;
      }
      _imageVisible = true;
    });
  }

  Widget _provideImagePlaceholder() {
    return SvgPicture.asset(
      'assets/images/avatar_placeholder.svg',
      width: widget.imageSide,
      height: widget.imageSide,
    );
  }

  _reloadState() {
    setState(() {
      _getName();
    });
  }

  _getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final name = prefs.getString(SharedPrefsKeys.userName) ?? "Ваше имя";
    setState(() {
      _name = name.isEmpty ? "Ваше имя" : name;
    });
  }

  @override
  void initState() {
    super.initState();
    _getName();
    _retrieveImage();
  }

  @override
  Widget build(BuildContext context) {
    widget.showMessageIfFirstLaunch(widget, Messages.profileVC, context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey[350],
              offset: Offset(0, 3),
              blurRadius: 12.0,
            ),
          ],
        ),
        child: Flex(
          direction: Axis.vertical,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    pickImage();
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(widget.imageSide / 2),
                    child: AnimatedOpacity(
                      duration: Duration(milliseconds: 100),
                      opacity: _imageVisible ? 1.0 : 0.0,
                      child: (_image != null)
                          ? Image.file(
                              _image,
                              width: widget.imageSide,
                              height: widget.imageSide,
                              fit: BoxFit.cover,
                            )
                          : _provideImagePlaceholder(),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                GestureDetector(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (_) => TextfieldAlert(
                        context: context,
                        title: 'Введите новое имя',
                        callback: _reloadState,
                      ),
                    );
                  },
                  child: AutoSizeText(
                    _name ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: TensesData.groupColors[5],
                      fontSize: 18,
                      fontFamily: "AvenirNext",
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ProgressCircle(1.0, 'Курс'),
                ProgressCircle(0.0, 'Задания'),
              ],
            ),
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 165),
              child: CupertinoButton(
                pressedOpacity: 0.7,
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Настройки',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 19,
                          fontFamily: "AvenirNext",
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(width: 10),
                    Icon(Icons.settings),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(22)),
                onPressed: () {
                  final settingsRoute = SettingsScreen(
                    model: SettingsModel(),
                  );
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => settingsRoute));
                },
                color: TensesData.groupColors[5],
              ),
            )
          ],
        ),
      ),
    );
  }
}
