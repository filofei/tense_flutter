import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/custom_widgets/onboarding_page.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/onboarding/onboarding_model.dart';
import 'package:page_view_indicators/page_view_indicators.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

class OnboardingScreen extends StatefulWidget {
  final OnboardingModel model;
  const OnboardingScreen({Key key, this.model}) : super(key: key);

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  List<Widget> slides = [];
  final _selectedPage = ValueNotifier<int>(0);

  _onDone() {
    Navigator.of(context).pop();
  }

  final _pageController = PageController(
    initialPage: 0,
  );

  Widget _provideBottomButton() {
    switch (_selectedPage.value) {
      case 2:
        return CupertinoButton(
          color: TensesData.groupColors[5],
          padding: EdgeInsets.all(8),
          borderRadius: BorderRadius.circular(30),
          child: Text(
            'Начнём!',
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w600,
              fontSize: 20,
              color: Colors.white,
            ),
          ),
          onPressed: _onDone,
        );
        break;
      default:
        return CupertinoButton(
          padding: EdgeInsets.all(8),
          child: Text(
            'Пропустить',
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: Colors.grey,
            ),
          ),
          onPressed: _onDone,
        );
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  _setPref() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    sharedPref.setBool(SharedPrefsKeys.onboardingDidShow, true);
  }

  @override
  void initState() {
    super.initState();
    _setPref();
    for (int i = 0; i < widget.model.numberOfPages; i++) {
      slides.add(
        OnboardingPage(
          title: widget.model.header(i),
          description: widget.model.description(i),
          image: widget.model.image(
            i,
            MediaQuery.of(context).size.height / 3,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Flex(
        direction: Axis.vertical,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: CarouselSlider(
              items: slides,
              options: CarouselOptions(
                enableInfiniteScroll: false,
                height: MediaQuery.of(context).size.height * 0.8,
                viewportFraction: 1.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _selectedPage.value = index;
                  });
                },
              ),
            ),
          ),
          CirclePageIndicator(
            currentPageNotifier: _selectedPage,
            itemCount: widget.model.numberOfPages,
            selectedDotColor: TensesData.groupColors[5],
            dotColor: Colors.grey[400],
            size: 10,
            selectedSize: 10,
            dotSpacing: 8,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: SizedBox(
              height: 50,
              width: 140,
              child: _provideBottomButton(),
            ),
          )
        ],
      ),
    );
  }
}
