import 'package:flutter_svg/flutter_svg.dart';
import 'package:tense/data/onboarding_data.dart';

class OnboardingModel {
  SvgPicture image(int index, double height) {
    return SvgPicture.asset(
      'assets/images/onboarding${index + 1}.svg',
      height: height,
    );
  }

  String header(int index) {
    return OnboardingData.headers[index];
  }

  String description(int index) {
    return OnboardingData.descriptions[index];
  }

  int get numberOfPages {
    return OnboardingData.headers.length;
  }
}
