import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:package_info/package_info.dart';
import 'package:tense/custom_widgets/settings/switch_cell.dart';
import 'package:tense/custom_widgets/top_bar.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/settings/settings_model.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

class SettingsScreen extends StatefulWidget {
  final SettingsModel model;
  final List<String> prefsKeys = [SharedPrefsKeys.sound, SharedPrefsKeys.push];
  SettingsScreen({Key key, this.model}) : super(key: key);

  static _sendMail() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final Email email = Email(
      body: """
          <br><br><br>----Техническая информация----<br>
          OS: ${androidInfo.version.release}<br>
          Device: ${androidInfo.model}<br>
          Version: ${packageInfo.version}<br>
          Build: ${packageInfo.buildNumber}<br>
          ----Пожалуйста, не удаляйте----
          """,
      subject: 'Связь с разработчиком',
      recipients: ['tensemailbox@gmail.com'],
      isHTML: true,
    );
    await FlutterEmailSender.send(email);
  }

  static _showInfoScreen() {
    print('Show info');
  }

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final List<Function> actions = const [
    SettingsScreen._sendMail,
    SettingsScreen._showInfoScreen
  ];
  List<bool> switchStates = [];

  Future<List<bool>> getSwitchStates() async {
    List<bool> output = [];
    await widget.model.soundState().then((value) => output.add(value));
    await widget.model.pushState().then((value) => output.add(value));
    return output;
  }

  @override
  void initState() {
    super.initState();
    getSwitchStates().then((value) {
      print("Value in getStates: $value");
      setState(() {
        if (value != null) {
          switchStates = value;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          TopBar(
            headerText: widget.model.title(),
            handler: () {
              Navigator.of(context).pop();
            },
          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context, int index) {
                if ([0, 1].contains(index)) {
                  final states =
                      (switchStates.isEmpty) ? [false, false] : switchStates;
                  print("States before cell init: $states");
                  return SwitchCell(
                    title: widget.model.titles()[index],
                    state:
                        true /*
                        (states[index] == null) ? false : switchStates[index]*/
                    ,
                    prefsKey: widget.prefsKeys[index],
                  );
                } else if ([2, 3].contains(index)) {
                  return Container(
                    height: 55,
                    child: MaterialButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          widget.model.titles()[index],
                          style: TextStyle(
                            fontFamily: 'AvenirNext',
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: TensesData.groupColors[5],
                          ),
                        ),
                        onPressed: actions[index - 2]),
                  );
                } else {
                  return SizedBox(
                    height: 0,
                  );
                }
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(
                  thickness: 0.5,
                  height: 0.5,
                  color: Colors.grey,
                );
              },
              itemCount: widget.model.numberOfRows,
            ),
          ),
        ],
      ),
    );
  }
}
