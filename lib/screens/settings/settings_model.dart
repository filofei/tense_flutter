import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

class SettingsModel {
  List<String> titles() {
    return [
      'Звук',
      'Напоминания',
      'Написать разработчику',
      'Информация',
    ];
  }

  Future<bool> _getValueFromPrefs(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  int get numberOfRows {
    return titles().length;
  }

  String title() {
    return 'Настройки';
  }

  Future<bool> soundState() async {
    return _getValueFromPrefs(SharedPrefsKeys.sound);
  }

  Future<bool> pushState() {
    return _getValueFromPrefs(SharedPrefsKeys.push);
  }
}
