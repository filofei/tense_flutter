import 'package:flutter/material.dart';
import 'package:tense/custom_widgets/course/course_tile.dart';
import 'package:tense/custom_widgets/tab_bar_top_bar.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/course/course_model.dart';
import 'package:tense/screens/subscriptions/subscription_model.dart';
import 'package:tense/screens/subscriptions/subscriptions_screen.dart';

class CourseScreen extends StatefulWidget with FirstLaunchMixin {
  final CourseModel model;

  CourseScreen({this.model});

  @override
  _CourseScreenState createState() => _CourseScreenState();
}

class _CourseScreenState extends State<CourseScreen> {
  @override
  Widget build(BuildContext context) {
    widget.showMessageIfFirstLaunch(widget, Messages.courseVC, context);
    return Container(
      child: Column(
        children: <Widget>[
          TabBarTopBar(
            handler: () {
              MaterialPageRoute route =
                  MaterialPageRoute(builder: (BuildContext context) {
                return SubscriptionsScreen(
                  model: SubscriptionsModel(),
                );
              });
              Navigator.of(context).push(route);
            },
            headerText: widget.model.title(),
          ),
          Expanded(
            flex: 2,
            child: ListView.separated(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 10),
              itemBuilder: (BuildContext context, int index) {
                return CourseTile(model: widget.model.tileModel(index));
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(height: 0);
              },
              itemCount: widget.model.numberOfRows(),
            ),
          ),
        ],
      ),
    );
  }
}
