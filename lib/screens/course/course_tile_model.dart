import 'dart:ui';

class CourseTileModel {
  final String title;
  final List<String> tenseTitles;
  final Color color;

  CourseTileModel(this.title, this.tenseTitles, this.color);
}
