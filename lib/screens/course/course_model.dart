import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/course/course_tile_model.dart';

class CourseModel {
  String title() {
    return 'Выберите курс';
  }

  int numberOfRows() {
    return TensesData.tenses.length;
  }

  CourseTileModel tileModel(int index) {
    final tileModel = CourseTileModel(
      TensesData.groupNames[index],
      TensesData.tenses[index].item1,
      TensesData.groupColors[index],
    );
    return tileModel;
  }
}
