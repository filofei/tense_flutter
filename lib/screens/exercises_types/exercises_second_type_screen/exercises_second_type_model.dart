import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:tense/data/exercises_data/second_type_data.dart';

class ExercisesSecondTypeModel {
  final String tenseName;
  final int type = 0;
  AssetsAudioPlayer audioPlayer;

  ExercisesSecondTypeModel(this.tenseName) {
    audioPlayer = AssetsAudioPlayer();
  }

  String title() {
    return tenseName;
  }

  String correctAnswer(int index) {
    return SecondTypeData.dataAsMap()[tenseName].item2[index].first;
  }

  List<String> provideAnswersArray(int index) {
    List<String> cutArray =
        List.from(SecondTypeData.dataAsMap()[tenseName].item2[index]);
    cutArray.shuffle();
    print("Shuffled array: $cutArray");
    return cutArray;
  }

  int numberOfQuestions() {
    return _questions().length;
  }

  List<String> _questions() {
    return SecondTypeData.dataAsMap()[tenseName].item1;
  }

  String question(int index) {
    return _questions()[index];
  }
}
