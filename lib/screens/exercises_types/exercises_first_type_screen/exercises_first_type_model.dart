import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:tense/data/exercises_data/first_type_data.dart';

class ExercisesFirstTypeModel {
  final String tenseName;
  final int type = 0;
  AssetsAudioPlayer audioPlayer;

  ExercisesFirstTypeModel(this.tenseName) {
    audioPlayer = AssetsAudioPlayer();
  }

  String title() {
    return tenseName;
  }

  String correctAnswer(int index) {
    return FirstTypeData.dataAsMap()[tenseName].item2[index].first;
  }

  List<String> provideAnswersArray(int index) {
    List<String> cutArray =
        List.from(FirstTypeData.dataAsMap()[tenseName].item2[index]);
    cutArray.shuffle();
    print("Shuffled array: $cutArray");
    return cutArray;
  }

  int numberOfQuestions() {
    return _questions().length;
  }

  List<String> _questions() {
    return FirstTypeData.dataAsMap()[tenseName].item1;
  }

  String question(int index) {
    return _questions()[index];
  }
}
