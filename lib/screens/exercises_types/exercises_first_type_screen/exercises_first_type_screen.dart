import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:tense/custom_widgets/fade_route.dart';
import 'package:tense/custom_widgets/top_bar.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:tense/mixins/exercises_mixin.dart';
import 'package:tense/screens/exercise_help/exercise_help_model.dart';
import 'package:tense/screens/exercise_help/exercise_help_screen.dart';
import 'package:tense/screens/exercises_types/exercises_first_type_screen/exercises_first_type_model.dart';

class ExercisesFirstTypeScreen extends StatefulWidget {
  final ExercisesFirstTypeModel model;

  const ExercisesFirstTypeScreen({Key key, this.model}) : super(key: key);

  @override
  _ExercisesFirstTypeScreenState createState() =>
      _ExercisesFirstTypeScreenState();
}

class _ExercisesFirstTypeScreenState extends State<ExercisesFirstTypeScreen>
    with SingleTickerProviderStateMixin, ExercisesMixin {
  Animation<Color> animation;
  AnimationController _animationController;
  int currentQuestion = 0;
  int mistakesCount = 0;
  double _progress = 0.0;
  List<String> answersArray = [];

  Widget _provideButtonsRow(int firstIndex) {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _provideButtonWidget(firstIndex),
          SizedBox(width: 1),
          _provideButtonWidget(firstIndex + 1),
        ],
      ),
    );
  }

  Widget _provideButtonWidget(int index) {
    String title = answersArray[index];
    return ConstrainedBox(
      constraints: BoxConstraints.expand(
          width: (MediaQuery.of(context).size.width / 2) - 0.5),
      child: MaterialButton(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          color: (title == widget.model.correctAnswer(currentQuestion))
              ? animation.value
              : TensesData.groupColors[5],
          child: AutoSizeText(
            title,
            textAlign: TextAlign.center,
            minFontSize: 14,
            maxLines: 3,
            style: TextStyle(
                fontFamily: 'AvenirNext',
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.white),
          ),
          onPressed: () {
            if (title == widget.model.correctAnswer(currentQuestion)) {
              widget.model.audioPlayer
                  .open(Audio('assets/sounds/rightAnswer.mp3'));
              widget.model.audioPlayer.play();
              if (currentQuestion <= widget.model.numberOfQuestions() - 1) {
                if (currentQuestion < widget.model.numberOfQuestions() - 1) {
                  setState(() {
                    currentQuestion += 1;
                    _progress += 1 / widget.model.numberOfQuestions();
                    answersArray =
                        widget.model.provideAnswersArray(currentQuestion);
                  });
                } else {
                  setState(() {
                    //currentQuestion += 1;
                    //_progress += 1 / widget.model.numberOfQuestions();
                    Timer(Duration(milliseconds: 300), () {
                      openResultScreen(mistakesCount, widget.model.type,
                          widget.model.tenseName, context);
                    });
                  });
                }
              }
            } else {
              widget.model.audioPlayer
                  .open(Audio('assets/sounds/wrongAnswer.mp3'));
              widget.model.audioPlayer.play();
              setState(() {
                mistakesCount += 1;
              });
            }
          }),
    );
  }

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 150));
    final CurvedAnimation curve =
        CurvedAnimation(parent: _animationController, curve: Curves.easeInOut);
    animation = ColorTween(
            begin: TensesData.groupColors[5], end: TensesData.groupColors[1])
        .animate(curve);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      }
    });
    answersArray = widget.model.provideAnswersArray(0);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          TopBar(
            headerText: widget.model.title(),
            handler: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    bottom: 4,
                    right: 0,
                    child: CupertinoButton(
                      padding: EdgeInsets.only(bottom: 16),
                      child: Icon(
                        Icons.help_outline,
                        size: 35,
                      ),
                      onPressed: () {
                        final routeModel = ExerciseHelpModel(
                            tenseName: widget.model.tenseName);
                        Navigator.push(
                            context,
                            FadeRoute(
                                page: ExerciseHelpScreen(model: routeModel)));
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(bottom: 4),
                      child: LinearPercentIndicator(
                        animation: true,
                        animateFromLastPercent: true,
                        animationDuration: 150,
                        padding: EdgeInsets.only(bottom: 8),
                        lineHeight: 7,
                        progressColor: TensesData.groupColors[5],
                        percent: _progress,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      widget.model.question(currentQuestion),
                      textAlign: TextAlign.center,
                      maxLines: 5,
                      style: TextStyle(
                          fontFamily: 'AvenirNext',
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: TensesData.groupColors[5]),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _provideButtonsRow(0),
                SizedBox(height: 1),
                _provideButtonsRow(2),
              ],
            ),
          )
        ],
      ),
    );
  }
}
