import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/course_test/course_test_model.dart';

class CourseTestScreen extends StatefulWidget {
  final CourseTestModel model;

  const CourseTestScreen({Key key, this.model}) : super(key: key);

  @override
  _CourseTestScreenState createState() => _CourseTestScreenState();
}

class _CourseTestScreenState extends State<CourseTestScreen> {
  Widget _progressBar(int index) {
    Color provideColor() {
      switch (currentProgress[index]) {
        case false:
          {
            return Color(0xFFD94C4C);
          }
          break;
        case true:
          {
            return TensesData.groupColors[1];
          }
          break;
        default:
          {
            return Colors.grey[300];
          }
      }
    }

    return Container(
      height: 8,
      width: (MediaQuery.of(context).size.width - 92) / 3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: provideColor(),
      ),
    );
  }

  int currentQuestion = 0;
  List<bool> currentProgress = [null, null, null];

  _onAnswerTap(bool buttonValue) {
    setState(() {
      if (currentQuestion <= widget.model.stepsCount() - 1) {
        currentProgress[currentQuestion] =
            (widget.model.answersList()[currentQuestion] == buttonValue);
      }
      currentQuestion += 1;
    });
  }

  Widget _provideBottomWidget(BuildContext context) {
    if (currentQuestion == widget.model.stepsCount()) {
      return ConstrainedBox(
        constraints: BoxConstraints.expand(
            height: 44, width: MediaQuery.of(context).size.width - 74),
        child: CupertinoButton(
          borderRadius: BorderRadius.circular(22),
          padding: EdgeInsets.zero,
          color: TensesData.groupColors[5],
          child: Text(
            'Окей',
            style: TextStyle(
                fontFamily: 'AvenirNext',
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: Colors.white),
          ),
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
        ),
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CupertinoButton(
            borderRadius: BorderRadius.circular(22),
            padding: EdgeInsets.symmetric(horizontal: 35),
            color: Color(0xFFD94C4C),
            child: Text(
              'Нет',
              style: TextStyle(
                  fontFamily: 'AvenirNext',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            onPressed: () {
              _onAnswerTap(false);
            },
          ),
          CupertinoButton(
            borderRadius: BorderRadius.circular(22),
            padding: EdgeInsets.symmetric(horizontal: 35),
            color: TensesData.groupColors[1],
            child: Text(
              'Да',
              style: TextStyle(
                  fontFamily: 'AvenirNext',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            onPressed: () {
              _onAnswerTap(true);
            },
          )
        ],
      );
    }
  }

  Widget _provideTextWidget() {
    if (currentQuestion < widget.model.stepsCount()) {
      return Text(widget.model.questionsList()[currentQuestion],
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: 'AvenirNext',
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.grey[800]));
    } else {
      return Text(widget.model.provideResultText(currentProgress),
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: 'AvenirNext',
              fontSize: 22,
              fontWeight: FontWeight.w600,
              color: TensesData.groupColors[5]));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.grey[350],
                offset: Offset(0, 3),
                blurRadius: 12.0,
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  widget.model.title(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 22,
                      fontFamily: 'AvenirNext',
                      fontWeight: FontWeight.w700,
                      color: Colors.grey[800]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _progressBar(0),
                    SizedBox(width: 10),
                    _progressBar(1),
                    SizedBox(width: 10),
                    _progressBar(2),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Align(
                    alignment: Alignment.center,
                    child: _provideTextWidget(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                child: AnimatedSwitcher(
                  duration: Duration(milliseconds: 400),
                  switchInCurve: Curves.easeIn,
                  switchOutCurve: Curves.easeOut,
                  child: _provideBottomWidget(context),
                  transitionBuilder:
                      (Widget child, Animation<double> animation) =>
                          ScaleTransition(
                    scale: animation,
                    child: child,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
