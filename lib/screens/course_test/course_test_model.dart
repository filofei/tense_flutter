import 'package:tense/data/course_test.dart';

class CourseTestModel {
  final String tenseName;

  CourseTestModel(this.tenseName);

  List<String> questionsList() {
    List<String> output = [];
    CourseTest.asMap.forEach((key, value) {
      if (key == tenseName) {
        value.forEach((element) {
          output.add(element.item1);
        });
      }
    });
    return output;
  }

  List<bool> answersList() {
    List<bool> output = [];
    CourseTest.asMap.forEach((key, value) {
      if (key == tenseName) {
        value.forEach((element) {
          output.add(element.item2);
        });
      }
    });
    return output;
  }

  String provideResultText(List<bool> result) {
    final bad = 'Стоит прочитать теорию снова.';
    final texts = ['Отлично!', 'Хорошо, но что-то упустили.', bad, bad];
    int mistakesCount = 0;
    result.forEach((element) {
      if (!element) {
        mistakesCount += 1;
      }
    });
    return texts[mistakesCount];
  }

  String title() {
    return 'Тест';
  }

  int stepsCount() {
    return questionsList().length;
  }
}
