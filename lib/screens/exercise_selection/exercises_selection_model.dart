class ExercisesSelectionModel {
  String tenseName;

  ExercisesSelectionModel({this.tenseName});

  List<String> _imageNames = [
    'assets/images/type1.svg',
    'assets/images/type2.svg',
    'assets/images/type3.svg',
  ];

  String title() {
    return 'Выберите тип задания';
  }

  String imageName(int index) {
    return _imageNames[index];
  }
}
