import 'package:flutter/material.dart';
import 'package:tense/custom_widgets/exercise_selection/exercise_selection_tile.dart';
import 'package:tense/custom_widgets/top_bar.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/exercises_types/exercises_first_type_screen/exercises_first_type_model.dart';
import 'package:tense/screens/exercises_types/exercises_first_type_screen/exercises_first_type_screen.dart';
import 'package:tense/screens/exercises_types/exercises_second_type_screen/exercises_second_type_model.dart';
import 'package:tense/screens/exercises_types/exercises_second_type_screen/exercises_second_type_screen.dart';

import 'exercises_selection_model.dart';

class ExerciseSelectionScreen extends StatelessWidget with FirstLaunchMixin {
  final ExercisesSelectionModel model;

  ExerciseSelectionScreen({this.model});

  Function _provideHandler(int type, BuildContext context) {
    Widget route;
    switch (type) {
      case 0:
        final routeModel = ExercisesFirstTypeModel(model.tenseName);
        route = ExercisesFirstTypeScreen(model: routeModel);
        break;
      case 1:
        final routeModel = ExercisesSecondTypeModel(model.tenseName);
        route = ExercisesSecondTypeScreen(model: routeModel);
        break;
      default:
    }
    return () {
      Navigator.push(context, MaterialPageRoute(builder: (context) => route));
    };
  }

  @override
  Widget build(BuildContext context) {
    showMessageIfFirstLaunch(this, Messages.typesOfExercisesVC, context);
    return Scaffold(
      body: Column(
        children: <Widget>[
          TopBar(
            headerText: model.title(),
            handler: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ExerciseSelectionTile(
                    assetName: model.imageName(0),
                    checked: false,
                    title: 'Вставить верное слово',
                    handler: _provideHandler(0, context),
                  ),
                  SizedBox(height: 25),
                  ExerciseSelectionTile(
                    assetName: model.imageName(1),
                    checked: true,
                    title: 'Выбрать верный перевод',
                    handler: _provideHandler(1, context),
                  ),
                  SizedBox(height: 25),
                  ExerciseSelectionTile(
                    assetName: model.imageName(2),
                    checked: false,
                    title: 'Дополнить перевод',
                    handler: _provideHandler(2, context),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 70),
        ],
      ),
    );
  }
}
