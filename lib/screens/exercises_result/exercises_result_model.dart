class ExercisesResultModel {
  final int mistakesCount;
  final int type;
  final String tenseName;

  ExercisesResultModel(this.mistakesCount, this.type, this.tenseName);

  String getResultText() {
    switch (mistakesCount) {
      case 0:
        return 'Идеально!';
        break;
      case 1:
        return 'Отлично!';
        break;
      case 2:
        return 'Хорошо!';
        break;
      default:
        return 'Попробуйте снова :(';
        break;
    }
  }

  String getMistakesLabel() {
    if (mistakesCount == 1) {
      return "1 ошибка";
    } else if (mistakesCount >= 2 && mistakesCount <= 4) {
      return "$mistakesCount ошибки";
    } else if (mistakesCount == 0 ||
        (mistakesCount >= 5 && mistakesCount <= 20)) {
      return "$mistakesCount ошибок";
    } else {
      return 'Много ошибок';
    }
  }

  String getImageName() {
    switch (mistakesCount) {
      case 0:
        return 'assets/images/perfect_mark.svg';
        break;
      case 1:
        return 'assets/images/perfect_mark.svg';
        break;
      case 2:
        return 'assets/images/good_mark.svg';
        break;
      default:
        return 'assets/images/bad_mark.svg';
        break;
    }
  }
}
