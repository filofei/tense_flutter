import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/exercises_result/exercises_result_model.dart';

class ExercisesResultScreen extends StatefulWidget {
  final ExercisesResultModel model;

  const ExercisesResultScreen({Key key, this.model}) : super(key: key);

  @override
  _ExercisesResultScreenState createState() => _ExercisesResultScreenState();
}

class _ExercisesResultScreenState extends State<ExercisesResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey[350],
              offset: Offset(0, 3),
              blurRadius: 12.0,
            ),
          ],
        ),
        child: Flex(
          direction: Axis.vertical,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.center,
                child: AutoSizeText(
                  widget.model.getResultText(),
                  minFontSize: 24,
                  style: TextStyle(
                    fontFamily: 'AvenirNext',
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                    color: TensesData.groupColors[5],
                  ),
                ),
              ),
            ),
            SvgPicture.asset(
              widget.model.getImageName(),
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.width * 0.5,
            ),
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  widget.model.getMistakesLabel(),
                  style: TextStyle(
                      fontFamily: 'AvenirNext',
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey[600]),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: (MediaQuery.of(context).size.width / 4),
              ),
              child: CupertinoButton(
                  color: TensesData.groupColors[5],
                  borderRadius: BorderRadius.circular(30),
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Окей',
                    style: TextStyle(
                        fontFamily: 'AvenirNext',
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  onPressed: () {
                    // TODO: Save result
                    Navigator.of(context).popUntil((route) => route.isFirst);
                  }),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  'Задание учитывается, если в нём не больше одной ошибки.',
                  maxLines: 2,
                  style: TextStyle(
                    fontFamily: 'AvenirNext',
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[700],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
