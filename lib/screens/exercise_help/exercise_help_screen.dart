import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tense/custom_widgets/exercise_help/exercise_help_ending_tile.dart';
import 'package:tense/custom_widgets/exercise_help/exercises_help_verb_tile.dart';
import 'package:tense/data/messages_data.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/mixins/first_launch_mixin.dart';
import 'package:tense/screens/exercise_help/exercise_help_model.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ExerciseHelpScreen extends StatefulWidget with FirstLaunchMixin {
  final ExerciseHelpModel model;

  ExerciseHelpScreen({this.model});

  @override
  _ExerciseHelpScreenState createState() => _ExerciseHelpScreenState();
}

class _ExerciseHelpScreenState extends State<ExerciseHelpScreen> {
  WebViewController _controller;

  bool justOpen = true;
  bool loadedHtml = false;
  int _selectedIndex = 0;
  String _hintUrl = 'about:blank';

  _loadHtmlFromAssets() async {
    await rootBundle
        .loadString('assets/course_pages/toBe3.html')
        .then((value) async {
      await _controller.loadUrl(Uri.dataFromString(
        value,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
        parameters: {'charset': 'utf-8'},
      ).toString());
    });
  }

  Map<dynamic, Widget> _provideSliderWidgets() {
    Map<dynamic, Widget> output = {};
    widget.model.sliderItems.forEach((key, value) {
      output[key as int] = _sliderWidget(
        value,
        key == _selectedIndex ? Colors.white : Colors.grey,
      );
    });
    return output;
  }

  Widget _provideMainWidget() {
    switch (_selectedIndex) {
      case 0:
        if (justOpen) {
          Timer(Duration(milliseconds: 1000), () {
            setState(() {
              justOpen = false;
            });
          });
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            child: Opacity(
              opacity: loadedHtml ? 1.0 : 0.0,
              child: WebView(
                initialUrl: _hintUrl,
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) async {
                  _controller = webViewController;
                  await _loadHtmlFromAssets();
                },
                onPageFinished: (_) {
                  Timer(Duration(milliseconds: 300), () {
                    setState(() {
                      loadedHtml = true;
                    });
                  });
                },
              ),
            ),
          );
        }
        break;
      case 1:
        return ListView.separated(
          padding: EdgeInsets.symmetric(vertical: 16),
          itemBuilder: (BuildContext context, int index) {
            return ExerciseHelpEndingTile(
              model: widget.model.endingTileModel(index),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
          itemCount: widget.model.endingsItemsCount(),
        );
        break;
      case 2:
        return ListView.separated(
          padding: EdgeInsets.symmetric(vertical: 16),
          itemBuilder: (BuildContext context, int index) {
            return ExercisesHelpVerbTile(
              model: widget.model.verbTileModel(index),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
          itemCount: widget.model.verbsItemsCount(),
        );
        break;
      default:
        return SizedBox(
          height: 0,
        );
    }
  }

  Widget _sliderWidget(String title, Color textColor) {
    return Container(
      height: 40,
      child: Center(
        child: Text(
          title,
          style: TextStyle(
              fontFamily: 'AvenirNext',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: textColor),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    widget.showMessageIfFirstLaunch(widget, Messages.helpModelVC, context);
    return Scaffold(
      backgroundColor: const Color(0xBC000000),
      body: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 10,
          sigmaY: 10,
        ),
        child: SafeArea(
          child: Flex(
            direction: Axis.vertical,
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: 80,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width - 100,
                        ),
                        // child: Opacity(
                        //   opacity: (_selectedIndex == 2) ? 1.0 : 0.0,
                        //   child: AbsorbPointer(
                        //     absorbing: (_selectedIndex == 2) ? false : true,
                        //     child: SearchBar(
                        //       minimumChars: 1,
                        //       onSearch: (text) {
                        //         print(text);
                        //       },
                        //       onItemFound: (item, int index) {
                        //         return ExercisesHelpVerbTile();
                        //       },
                        //       searchBarStyle: SearchBarStyle(
                        //         padding: EdgeInsets.fromLTRB(12, 2, 10, 2),
                        //         backgroundColor: Colors.white24,
                        //       ),
                        //       textStyle: TextStyle(
                        //           fontFamily: 'AvenirNext',
                        //           fontSize: 19,
                        //           fontWeight: FontWeight.w500,
                        //           color: Colors.white),
                        //       shrinkWrap: true,
                        //     ),
                        //   ),
                        // ),
                      ),
                      CupertinoButton(
                        padding: EdgeInsets.zero,
                        child: Icon(
                          Icons.close,
                          size: 40,
                          color: TensesData.groupColors[5],
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.fromLTRB(20, 10, 20, 30),
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 70, 8, 0),
                        child: _provideMainWidget(),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 20,
                          vertical: 16,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 3,
                              offset: Offset(0.0, 3.0),
                            ),
                          ],
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(15),
                          ),
                        ),
                        child: CupertinoSlidingSegmentedControl(
                          groupValue: _selectedIndex,
                          thumbColor: TensesData.groupColors[5],
                          padding: EdgeInsets.zero,
                          children: _provideSliderWidgets(),
                          onValueChanged: (value) {
                            setState(() {
                              loadedHtml = false;
                              _selectedIndex = value as int;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
