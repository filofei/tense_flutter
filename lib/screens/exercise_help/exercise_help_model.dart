import 'package:flutter/material.dart';
import 'package:tense/data/exercises_hint_data.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/exercise_help/verb_tile_model.dart';

import 'ending_tile_model.dart';

class ExerciseHelpModel {
  String tenseName;

  ExerciseHelpModel({this.tenseName});

  Map<dynamic, String> sliderItems = {
    0: 'Правило',
    1: 'Окончания',
    2: 'Глаголы'
  };

  int endingsItemsCount() {
    return Hints.endings.length;
  }

  int verbsItemsCount() {
    return Hints.elements.length;
  }

  VerbTileModel verbTileModel(int index) {
    return VerbTileModel(
      Hints.elements[index],
      Hints.v1[index],
      Hints.v2[index],
      Hints.v3[index],
    );
  }

  EndingTileModel endingTileModel(int index) {
    Color color;
    switch (Hints.endings[index]) {
      case '+s':
        color = TensesData.groupColors[5];
        break;
      case '+ed':
        color = TensesData.groupColors[1];
        break;
      case '+ing':
        color = TensesData.groupColors[4];
        break;
      default:
        color = Colors.grey;
        break;
    }
    return EndingTileModel(
      Hints.endings[index],
      Hints.rules[index],
      Hints.samples[index],
      color,
    );
  }

  String hintUrl() {
    return '${Hints.hintsList[tenseName]}.html';
  }
}
