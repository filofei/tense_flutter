import 'package:flutter/material.dart';

class EndingTileModel {
  final String ending;
  final String rule;
  final String sample;
  final Color color;

  EndingTileModel(this.ending, this.rule, this.sample, this.color);
}
