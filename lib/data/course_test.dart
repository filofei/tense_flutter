import 'package:tuple/tuple.dart';

abstract class CourseTest {
  static const basics = [
    Tuple2("Подлежащее влияет на форму вспомогательного глагола?", true),
    Tuple2("В английском языке столько же времён, сколько и в русском?", false),
    Tuple2("Любое время состоит из значения, строения и слов-подсказок?", true)
  ];
  static const toBe = [
    Tuple2("Глагол 'to be' переводится как 'делать'?", false),
    Tuple2("'Was, were' это формы глагола 'to be' в прошедшем времени?", true),
    Tuple2(
        "Чтобы задать вопрос с глаголом 'to be', надо поставить глагол на первое место в предложении?",
        true)
  ];
  static const presentSimple = [
    Tuple2("Present Simple - это прошедшее время?", false),
    Tuple2(
        "Если подлежащее - 'я', 'мы', 'вы', то к основному глаголу добавляется '-s'?",
        false),
    Tuple2("Общие вопросы в Present Simple начинаются с 'Do' или 'Does'?", true)
  ];
  static const pastSimple = [
    Tuple2("В Past Simple используется вторая форма глагола?", true),
    Tuple2("Past Simple относится к группе 'простых' времён?", true),
    Tuple2("Past Simple используется для описания будущего?", false)
  ];
  static const futureSimple = [
    Tuple2(
        "С помощью Future Simple можно описать не только будущее,  но и настоящее?",
        false),
    Tuple2("'Will' - вспомогательный глагол в Future Simple?", true),
    Tuple2("Основной глагол в Future Simple стоит во второй форме?", false)
  ];
  static const presentContinuous = [
    Tuple2("Present Continuous описывает завершённое действие?", false),
    Tuple2(
        "В Present Continuous к основному глаголу всегда добавляется окончание '-ing'?",
        true),
    Tuple2("В Present Continuous нет слов-подсказок?", false)
  ];
  static const pastContinuous = [
    Tuple2(
        "Past Continuous нужно, чтобы рассказать о процессе в прошлом?", true),
    Tuple2(
        "В Past Continuous к основному глаголу не добавляется никаких окончаний?",
        false),
    Tuple2(
        "Если подлежащее 'you', 'we', 'they', то вспомогательный глагол - 'were'?",
        true)
  ];
  static const futureContinuous = [
    Tuple2("'Future Continuous' переводится как 'будущее совершённое'?", false),
    Tuple2("Future Continuous описывает процесс?", true),
    Tuple2("'Will be' - вспомогательный глагол в Future Continuous?", true)
  ];
  static const presentPerfect = [
    Tuple2("'Perfect' означает 'длительное'?", false),
    Tuple2("Действие в Present Perfect происходит прямо сейчас?", false),
    Tuple2("Основной глагол в Present Perfect стоит во 2-ой форме?", false)
  ];
  static const pastPerfect = [
    Tuple2("Past Perfect относится к группе совершённых времён?", true),
    Tuple2("Вспомогательный глагол в Past Perfect - 'Haved'?", false),
    Tuple2("В Past Perfect нет слов-подсказок?", false)
  ];
  static const futurePerfect = [
    Tuple2("Это время описывает процесс, который будет идти в будущем?", false),
    Tuple2(
        "Вспомогательные глаголы в Future Perfect - 'Will have' и 'Will has'?",
        false),
    Tuple2("'Now', 'currently' - слова-подсказки в Future Perfect?", false)
  ];
  static const presentPerfectContinuous = [
    Tuple2(
        "Данное время описывает процесс, который длился до настоящего или имеет результат в настоящем?",
        true),
    Tuple2(
        "Основной глагол во временах Perfect Continuous всегда имеет окончание '-ing'?",
        true),
    Tuple2(
        "Слова-подсказки в данном времени такие же, как и в Present Continuous?",
        false)
  ];
  static const pastPerfectContinous = [
    Tuple2("У основного глагола в данном времени окончание 'ing'?", true),
    Tuple2("Вспомогательный глагол в этом времени - 'Had be'?", false),
    Tuple2(
        "Слова-подсказки в Past Perfect Continuous - 'For', 'Since', 'All...'?",
        true)
  ];
  static const futurePerfectContinuous = [
    Tuple2("Future Perfect Continuous - прошедшее время?", false),
    Tuple2(
        "В отрицательных предложениях в этом времени 'not' стоит после 'will'?",
        true),
    Tuple2("Данное время редко используется?", true)
  ];
  static const passiveSimple = [
    Tuple2(
        "Пассивный залог - это когда над кем-то или чем-то совершается действие?",
        true),
    Tuple2("В пассивном залоге основной глагол всегда во второй форме?", false),
    Tuple2(
        "Во временах группы Simple в пассивном залоге вспомогательный глагол — 'to be' в разных формах?",
        true)
  ];
  static const passiveContinuous = [
    Tuple2(
        "Have/Has + being — вспомогательные глаголы в группе Continuous пассивного залога?",
        false),
    Tuple2("Future Continuous в пассивном залоге не существует?", true),
    Tuple2(
        "Времена Continuous в пассивном залоге описывают процесс, который совершался или совершается над кем-то/чем-то?",
        true)
  ];
  static const passivePerfect = [
    Tuple2("Future Perfect Passive не существует?", false),
    Tuple2(
        "'Being' является частью вспомогательного глагола в этой группе времён?",
        false),
    Tuple2(
        "В данной группе времён такие же слова-подсказки, как и во временах Perfect активного залога?",
        true)
  ];
  static const futureInThePast = [
    Tuple2(
        "В Future-in-the-Past мы ссылаемся на будущее из какого-то момента в прошлом?",
        true),
    Tuple2(
        "По строению, времена этой группы отличаются от обычных времён Future только глаголом 'would' вместо 'will'?",
        true),
    Tuple2("Future-in-the-Past Continuous не существует?", false)
  ];
  static const sequenceOfTenses = [
    Tuple2(
        "Согласование времён применимо только к сложноподчинённым предложениям?",
        true),
    Tuple2(
        "При согласовании времён время в придаточном предолжении сдвигается в прошлое?",
        true),
    Tuple2(
        "Если главное предложение стоит в настоящем времени, то мы также должны делать согласование времён?",
        false)
  ];

  static const Map<String, List<Tuple2<String, bool>>> asMap = {
    "Что такое времена?": basics,
    "Глагол 'to be'": toBe,
    "Present Simple": presentSimple,
    "Past Simple": pastSimple,
    "Future Simple": futureSimple,
    "Present Continuous": presentContinuous,
    "Past Continuous": pastContinuous,
    "Future Continuous": futureContinuous,
    "Present Perfect": presentPerfect,
    "Past Perfect": pastPerfect,
    "Future Perfect": futurePerfect,
    "Present Perfect Continuous": presentPerfectContinuous,
    "Past Perfect Continuous": pastPerfectContinous,
    "Future Perfect Continuous": futurePerfectContinuous,
    "Simple": passiveSimple,
    "Continuous": passiveContinuous,
    "Perfect": passivePerfect,
    "Future-in-the-Past": futureInThePast,
    "Согласование времён": sequenceOfTenses,
  };
}
