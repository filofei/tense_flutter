abstract class Messages {
  static const helpModelVC =
      'Здесь находятся все правила и список неправильных глаголов.';
  static const profileVC =
      'Здесь вы можете отслеживать ваш прогресс и настроить свой профиль.';
  static const typesOfExercisesVC =
      'Здесь три типа заданий. "Дополнить перевод" — повышенной сложности.';
  static const exercisesVC =
      'Здесь находятся задания по всем временам из курса теории.';
  static const courseVC =
      'На этом экране находится курс теории по всем временам.';
  static const coursePageVC =
      'Внимательно читайте теорию. В конце вас ждёт тест.';
  static const coursePageVCFirstFinish =
      'Вы прошли свой первый курс теории. Не забывайте выполнять по ним задания.';
}
