import 'package:tuple/tuple.dart';
import '../tenses_data.dart';

abstract class SecondTypeData {
  static Map<String, Tuple2<List<String>, List<List<String>>>> dataAsMap() {
    Map<String, Tuple2<List<String>, List<List<String>>>> output = {};
    List<String> cutArray = TensesData.tensesAsArray();
    cutArray.removeAt(0);
    cutArray.asMap().forEach((index, element) {
      output[element] = data[index];
    });
    return output;
  }

  static const List<Tuple2<List<String>, List<List<String>>>> data = [
    // DONE!
    Tuple2(
        /* + */ [
          "Тот мужчина инженер.",
          "Эти котята серые.",
          "Мы не были в музее.",
          "Это будет отлично!",
          /* - */ "Эта девушка не официантка.",
          "Это не было моей ошибкой.",
          "Эти дети не были там.",
          "Они не будут в городе завтра.",
          /* ? */ "Как вас зовут?",
          "Какой у него был адрес?",
          "Джессика будет дома вечером?",
          "Что это будет?",
        ], [
      /* + */ [
        "That man is an engineer.",
        "That man are an engineer.",
        "That man does an engineer."
      ],
      [
        "These kittens are grey.",
        "These kittens do grey.",
        "These kittens have grey."
      ],
      [
        "We weren't at the museum.",
        "We wasn't at the museum.",
        "We didn't be at the museum."
      ],
      ["That will be great!", "That is great!", "That will great!"],
      /* - */ [
        "This girl isn't a waitress.",
        "This girl aren't a waitress.",
        "This girl am not a waitress."
      ],
      [
        "This wasn't my mistake.",
        "This weren't my mistake.",
        "This isn't my mistake."
      ],
      [
        "These children weren't there.",
        "These children wasn't there.",
        "These children isn't there."
      ],
      [
        "They won't be in town tomorrow.",
        "They won't in town tomorrow.",
        "They willn't be in town tomorrow."
      ],
      /* ? */ [
        "What is your name?",
        "What was your name?",
        "What does your name?"
      ],
      ["What was his address?", "What am his address?", "What is his address?"],
      [
        "Will Jessica be at home tonight?",
        "Will Jessica at home tonight?",
        "Will Jessica been at home tonight?"
      ],
      ["What will it be?", "What will it is?", "What will it will?"],
    ]),

    Tuple2(
        /* + */ [
          "Я работаю на фабрике.",
          "Она любит кошек.",
          "Мы часто гуляем в саду.",
          "Стив иногда приходит к нам.",
          /* - */ "Они не читают книги очень часто.",
          "Вы не пьете кофе.",
          "Обычно, отец не ходит в офис пешком.",
          "Мой брат не играет в компьютер.",
          /* ? */ "Где ты играешь в футбол?",
          "Вы предпочитаете американо?",
          "Мэри любит драмы или комедии?",
          "Когда он катается на лошади?",
        ], [
      /* + */ [
        "I work in a factory.",
        "I works in a factory.",
        "I am work in a factory."
      ],
      ["She likes cats.", "She like cats.", "She is likes cats."],
      [
        "We often walk in the garden.",
        "We often walks in the garden",
        "We are walk in the garden."
      ],
      [
        "Sometimes Steve comes to us.",
        "Sometimes Steve come to us.",
        "Sometimes Steve is coming to us."
      ],
      /* - */ [
        "They don't read books very often.",
        "They doesn't read books very often.",
        "They don't reads books very often."
      ],
      [
        "You don't drink coffee.",
        "You don't drinks coffee.",
        "You aren't drink coffee."
      ],
      [
        "Usually the father doesn't walk to his office.",
        "Usually the father don't walks to his office.",
        "Usually the father isn't walk to his office."
      ],
      [
        "My brother doesn't play computer games.",
        "My brother don't plays computer games.",
        "My brother isn't play computer games."
      ],
      /* ? */ [
        "Where do you play football?",
        "Where does you play football?",
        "Where are you play football?"
      ],
      [
        "Do you prefer americano?",
        "Does you prefer americano?",
        "Do you prefers americano?"
      ],
      [
        "Does Mary like drama or comedy movies?",
        "Does Mary likes drama or comedy movies?",
        "Do Mary likes drama or comedy movies?"
      ],
      [
        "When does he ride a horse?",
        "When do he rides a horse?",
        "When does he rides a horse?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Миссис Смит полила растения вчера.",
          "Мы слушали музыку вечером.",
          "Марта испекла вкусный пирог 2 часа назад.",
          "Кошка поела утром.",
          /* - */ "Я не ездил в Москву на прошлой неделе.",
          "Ник не получил '5'.",
          "Я не смотрел вчера телевизор.",
          "У меня не было велосипеда в детстве.",
          /* ? */ "Что они делали вчера?",
          "Где он купил машину?",
          "Почему автобус прибыл так поздно?",
          "Он выпил молоко?",
        ], [
      /* + */ [
        "Mrs. Smith watered the plants yesterday.",
        "Mrs. Smith did watered the plants yesterday.",
        "Mrs. Smith water the plants yesterday."
      ],
      [
        "We listened to music in the evening.",
        "We listen to music in the evening.",
        "We did listened to music in the evening."
      ],
      [
        "Martha baked a delicious cake 2 hours ago.",
        "Martha bakeed a delicious cake 2 hours ago.",
        "Martha did baked a delicious cake 2 hours ago."
      ],
      [
        "The cat ate in the morning.",
        "The cat eat in the morning.",
        "The cat eated in the morning."
      ],
      /* - */ [
        "I didn't go to Moscow last week.",
        "I didn't went to Moscow last week.",
        "I not go to Moscow last week."
      ],
      ["Nick didn't get '5'.", "Nick didn't got '5'.", "Nick don't got '5'."],
      [
        "I didn't watch TV yesterday.",
        "I didn't watched TV yesterday.",
        "I not watched TV yesterday."
      ],
      [
        "I didn't have a bike in the childhood.",
        "I didn't haved a bike in the childhood.",
        "I didn't had a bike in the childhood."
      ],
      /* ? */ [
        "What did they do yesterday?",
        "What did they did yesterday?",
        "What did they yesterday?"
      ],
      [
        "Where did he buy the car?",
        "Where did he bought the car?",
        "Where did he buyed the car?"
      ],
      [
        "Why did the bus arrive so late?",
        "Why did the bus arrived so late?",
        "Why did the bus arroved so late?"
      ],
      [
        "Did he drink the milk?",
        "Did he drinked the milk?",
        "He drinked the milk?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Завтра будет дождь.",
          "Мы посетим музей на следующей неделе.",
          "Я пойду к зубному в следующем месяце.",
          "Он станет знаменитым.",
          /* - */ "Я не буду есть это.",
          "Мы не будем жить в этом доме.",
          "Сэм не будет делать уроки завтра.",
          "Дети не будут играть в хоккей на следующей неделе.",
          /* ? */ "Где он будет спать?",
          "Вы будете завтракать?",
          "Ты принесешь мне журнал?",
          "Что она будет делать вечером?",
        ], [
      /* + */ [
        "It will rain tomorrow.",
        "It will rains tomorrow.",
        "It rains tomorrow."
      ],
      [
        "We will visit the museum next week.",
        "We will visiting the museum next week.",
        "We will be visit the museum next week."
      ],
      [
        "I will go to the dentist next month.",
        "I will be go to the dentist next month.",
        "I am go to the dentist next month."
      ],
      [
        "He will become famous.",
        "He will becomes famous.",
        "He is become famous."
      ],
      /* - */ ["I won't eat it.", "I won't eating it.", "I am not eat it."],
      [
        "We won't live in this house.",
        "We won't be live in this house.",
        "We aren't live in this house."
      ],
      [
        "Sam won't do homework tomorrow.",
        "Sam won't be do homework tomorrow.",
        "Sam won't does homework tomorrow."
      ],
      [
        "The children won't play hockey next week.",
        "The children won't be play hockey next week.",
        "The children won't playing hockey next week."
      ],
      /* ? */ [
        "Where will he sleep?",
        "Where is he sleep?",
        "Where will he sleeps?"
      ],
      [
        "Will you have breakfast?",
        "Will you having breakfast?",
        "Will you be have breakfast?"
      ],
      [
        "Will you bring me the magazine?",
        "Will you brings me the magazine?",
        "Are you bring me the magazine?"
      ],
      [
        "What will she do in the evening?",
        "What will she doing in the evening?",
        "What will she does in the evening?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Щенки играют на траве.",
          "Люси сейчас смотрит интересный фильм.",
          "Я сейчас ужинаю.",
          "Джон говорит по телефону в данный момент.",
          /* - */ "Мы сейчас не работаем в саду.",
          "Она сейчас не делает задания.",
          "Я не ношу куртку сейчас.",
          "Дети сейчас не едят суп.",
          /* ? */ "Вы сейчас едете домой?",
          "Они сейчас пьют кофе?",
          "Художник сейчас работает над своей картиной?",
          "Что вы делаете в данный момент?",
        ], [
      /* + */ [
        "The puppies are playing on the grass now.",
        "The puppies is playing on the grass now.",
        "The puppies are play on the grass now."
      ],
      [
        "Lucy is watching an interesting movie now.",
        "Lucy is watches an interesting movie now.",
        "Lucy are watching an interesting movie now."
      ],
      [
        "I am having dinner now.",
        "I is having dinner now.",
        "I are having dinner now."
      ],
      [
        "John is talking on the phone now.",
        "John am talking on the phone now.",
        "John is talks on the phone now."
      ],
      /* - */ [
        "We aren't working in the garden now.",
        "We isn't working in the garden now.",
        "We not working in the garden now."
      ],
      [
        "She isn't doing tasks now.",
        "She aren't doing tasks now.",
        "She doesn't doing tasks now."
      ],
      [
        "I am not wearing the jacket now.",
        "I do not wearing the jacket now.",
        "I is not wearing the jacket now."
      ],
      [
        "The children aren't eating soup now.",
        "The children isn't eating soup now.",
        "The children don't eat soup now."
      ],
      /* ? */ [
        "Are you driving home now?",
        "Is you driving home now?",
        "Do you driving home now?"
      ],
      [
        "Are they drinking coffee now?",
        "Is they drinking coffee now?",
        "Do they drinking coffee now?"
      ],
      [
        "Is the artist working on his drawing now?",
        "Are the artist working on his drawing now?",
        "Does the artist working on his drawing now?"
      ],
      [
        "What are you doing at the moment?",
        "What do you do at the moment?",
        "What is you doing at the moment?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Я плавал в бассейне в три часа дня.",
          "Он ремонтировал свою машину целое утро.",
          "Венди одевалась целый час.",
          "Мы играли в футбол с двух до пяти.",
          /* - */ "Она не спала с полуночи и до раннего утра.",
          "Я не спал в это время вчера.",
          "Солнце не сияло.",
          "Я не работал, когда мне позвонил друг.",
          /* ? */ "Что вы делали в это время позавчера?",
          "Она читала все утро?",
          "Джек сидел на диване, когда вы вошли?",
          "Вы играли на пианино с семи и до девяти вечера?",
        ], [
      /* + */ [
        "I was swimming in the pool at 3 pm.",
        "I were swimming in the pool at 3 pm.",
        "I swimmed in the pool at 3 pm."
      ],
      [
        "He was fixing his car the whole morning.",
        "He were fixing his car the whole morning.",
        "He fixing his car the whole morning."
      ],
      [
        "Wendy was dressing for about an hour.",
        "Wendy were dressing for about an hour.",
        "Wendy was dressed for about an hour."
      ],
      [
        "We were playing football from 2 pm till 5 pm.",
        "We are playing football from 2 pm till 5 pm.",
        "We was playing football from 2 pm till 5 pm."
      ],
      /* - */ [
        "She wasn't sleeping from the midnight till the early morning.",
        "She weren't sleeping from the midnight till the early morning.",
        "She not sleeping from the midnight till the early morning."
      ],
      [
        "I wasn't sleeping at this time yesterday.",
        "I weren't sleeping at this time yesterday.",
        "I wasn't slept at this time yesterday."
      ],
      [
        "The sun wasn't shining.",
        "The sun weren't shining.",
        "The sun isn't shining."
      ],
      [
        "I wasn't working when my friend called me.",
        "I weren't working when my friend called me.",
        "I isn't working when my friend called me."
      ],
      /* ? */ [
        "What were you doing at this time the day before yesterday?",
        "What was you doing at this time the day before yesterday?",
        "What are you doing at this time the day before yesterday?"
      ],
      [
        "Was she reading the whole morning?",
        "Did she read the whole morning?",
        "Were she reading the whole morning?"
      ],
      [
        "Was Jack sitting on the sofa when you entered?",
        "Were Jack sitting on the sofa when you entered?",
        "Is Jack sitting on the sofa when you entered?"
      ],
      [
        "Were you playing the piano from 7 pm till 9 pm?",
        "Was you playing the piano from 7 pm till 9 pm?",
        "Are you played the piano from 7 pm till 9 pm?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Я буду ждать тебя здесь в это же время завтра.",
          "Он будет играть в футбол с пяти до шести вечера.",
          "Когда ты вернешься домой, я буду все еще сидеть в самолете.",
          "Салли будет смотреть фильм с друзьями весь вечер.",
          /* - */ "Мы не будем работать здесь с 3 до 4 часов дня.",
          "Я не буду учиться целый день.",
          "Он не будет спать в это время завтра.",
          "Они не будут красить стены завтра утром.",
          /* ? */ "Будет ли Майкл играть в волейбол весь вечер?",
          "Вы будете лететь в Нью-Йорк завтра в это время?",
          "Где они будут гулять завтра весь вечер?",
          "Что будет Молли делать завтра с пяти до семи вечера?",
        ], [
      /* + */ [
        "I will be waiting for you here at this time tomorrow.",
        "I will be wait for you here at this time tomorrow.",
        "I will waiting for you here at this time tomorrow."
      ],
      [
        "He will be playing football from 5 pm till 6 pm.",
        "He will is playing football from 5 pm till 6 pm.",
        "He would be playing football from 5 pm till 6 pm."
      ],
      [
        "When you get home, I will still be sitting in the jet.",
        "When you get home, I will still sitting in the jet.",
        "When you get home, I will be still sit in the jet."
      ],
      [
        "Sally will be watching the film with her friends the whole evening.",
        "Sally will watch the film with her friends the whole evening.",
        "Sally will being watch the film with her friends the whole evening."
      ],
      /* - */ [
        "We won't be working here from 3 till 4 pm.",
        "We won't be work here from 3 till 4 pm.",
        "We won't be worked here from 3 till 4 pm."
      ],
      [
        "I won't be studying the whole day.",
        "I won't be study the whole day.",
        "I won't studying the whole day."
      ],
      [
        "He won't be sleeping at this time tomorrow.",
        "He won't sleeping at this time tomorrow.",
        "He won't be slept at this time tomorrow."
      ],
      [
        "They won't be painting the walls tomorrow morning.",
        "They won't being paint the walls tomorrow morning.",
        "They won't be painted the walls tomorrow morning."
      ],
      /* ? */ [
        "Will Michael be playing volleyball the whole evening?",
        "Will Michael playing volleyball the whole evening?",
        "Will Michael is playing volleyball the whole evening?"
      ],
      [
        "Will you be flying to NYC at this time tomorrow?",
        "Will you are flying to NYC at this time tomorrow?",
        "Will you do flying to NYC at this time tomorrow?"
      ],
      [
        "Where will they be walking the whole evening?",
        "Where will they be walk the whole evening?",
        "Where will they walking the whole evening?"
      ],
      [
        "What will Molly be doing tomorrow from 5 pm till 7 pm?",
        "What will Molly doing tomorrow from 5 pm till 7 pm?",
        "What is Molly be doing tomorrow from 5 pm till 7 pm?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Мальчик только что позавтракал.",
          "Софи уже пришла к ним.",
          "Дети недавно вернулись домой.",
          "Я потерял ключ и не могу войти в квартиру.",
          /* - */ "Он еще не видел этот фильм.",
          "Они никогда не были в Риме.",
          "Виктор еще не говорил с ними.",
          "Я не посещал музей сегодня.",
          /* ? */ "Вы когда-нибудь были в Чикаго?",
          "Что Мэри сказала?",
          "Он уже закончил свой проект?",
          "Ты когда-нибудь путешествовал на самолете?",
        ], [
      /* + */ [
        "The boy has just had breakfast.",
        "The boy have just had breakfast.",
        "The boy had just had breakfast."
      ],
      [
        "Sophie has already come to their place.",
        "Sophie did already come to their place.",
        "Sophie has already came to their place."
      ],
      [
        "The children have recently returned home.",
        "The children has recently returned home.",
        "The children have recently returnt home."
      ],
      [
        "I have lost my key and can’t enter the house.",
        "I had lost my key and can’t enter the house.",
        "I did lose my key and can’t enter the house."
      ],
      /* - */ [
        "He hasn’t seen this film yet.",
        "He didn’t see this film yet.",
        "He haven’t seen this film yet."
      ],
      [
        "They have never been to Rome.",
        "They had never been to Rome.",
        "They has never been to Rome."
      ],
      [
        "Viktor hasn’t spoken with them yet.",
        "Viktor didn’t spoke with them yet.",
        "Viktor didn’t speak with them yet."
      ],
      [
        "I haven’t visited the museum today.",
        "I hasn’t visited the museum today.",
        "I didn’t visited the museum today."
      ],
      /* ? */ [
        "Have you ever been to Chicago?",
        "Had you ever been to Chicago?",
        "Has you ever been to Chicago?"
      ],
      ["What has Mary said?", "What did Mary said?", "What did Mary sayed?"],
      [
        "Has he finished his project yet?",
        "Was he finished his project yet?",
        "Had he finished his project yet?"
      ],
      [
        "Have you ever traveled by plane?",
        "Has you ever traveled by plane?",
        "Did you ever travel by plane?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Я заснул до того, как закончил читать книгу.",
          "Она пообедала перед тем, как идти гулять.",
          "Я извинился за то, что пришел поздно.",
          "Мы закончили красить пол к 3 часам дня.",
          /* - */ "Майк не закончил свой проект к вчерашнему вечеру.",
          "Он сказал, что не купил еду для вечеринки.",
          "Я не пошел гулять до того, как кончился дождь.",
          "Мы не вернулись домой к 5 часам вечера.",
          /* ? */ "Вы добрались до Бостона к 10 часам утра?",
          "Он закончил играть в компьютер до того, как пришел брат?",
          "Люси дочитала книгу к началу прошлой недели?",
          "Вы пришли в кинотеатр до того, как фильм начался?",
        ], [
      /* + */ [
        "I had fallen asleep before I finished reading the book.",
        "I have fallen asleep before I finished reading the book.",
        "I has fallen asleep before I finished reading the book."
      ],
      [
        "She had had a dinner before she went for a walk.",
        "She did had a dinner before she went for a walk.",
        "She did have a dinner before she went for a walk."
      ],
      [
        "I apologized for the fact that I had come so late.",
        "I apologized for the fact that I have come so late.",
        "I apologized for the fact that I had came so late."
      ],
      [
        "We had finished painting the floor by 3 pm.",
        "We have finished painting the floor by 3 pm.",
        "We had been finished painting the floor by 3 pm."
      ],
      /* - */ [
        "Mike hadn't completed his project by yesterday evening.",
        "Mike haven't completed his project by yesterday evening.",
        "Mike hadn't complete his project by yesterday evening."
      ],
      [
        "He said that he hadn't bought the food for the party.",
        "He said that he haven't bought the food for the party.",
        "He said that he hadn't buyed the food for the party."
      ],
      [
        "I hadn't gone for a walk before the rain stopped.",
        "I hadn't went for a walk before the rain stopped.",
        "I hadn't go for a walk before the rain stopped."
      ],
      [
        "We hadn't returned home by 5 pm.",
        "We haven't returned home by 5 pm.",
        "We hasn't returned home by 5 pm."
      ],
      /* ? */ [
        "Had you got to Boston by 10 am?",
        "Have you got to Boston by 10 am?",
        "Had you get to Boston by 10 am?"
      ],
      [
        "Had he finished playing computer games before his brother came?",
        "Has he finished playing computer games before his brother came?",
        "Have he finished playing computer games before his brother came?"
      ],
      [
        "Had Lucy read the book to the end by the beginning of the last week?",
        "Had Lucy readed the book to the end by the beginning of the last week?",
        "Had Lucy reading the book to the end by the beginning of the last week?"
      ],
      [
        "Had you come to the cinema before the film started?",
        "Have you come to the cinema before the film started?",
        "Had you came to the cinema before the film started?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Это закончится к 5 часам вечера.",
          "Мы вернемся в Лондон к началу следующей недели.",
          "Папа закончит свою работу к 9 часам.",
          "Он откроет свой магазин к концу месяца.",
          /* - */ "Мы не съедим этот торт до конца праздника.",
          "Через месяц я не буду учителем уже как 2 года.",
          "Он не потратит все деньги до конца месяца.",
          "Марк не выучит свой урок до конца дня.",
          /* ? */ "Ты закончишь работу, прежде чем он приедет?",
          "Твой брат приедет сюда ко вторнику?",
          "Она уедет, прежде чем представление начнется?",
          "Фильм закончится к 10 часам вечера?",
        ], [
      /* + */ [
        "It will have finished by 5 pm.",
        "It will has finished by 5 pm.",
        "It will have finish by 5 pm."
      ],
      [
        "We will have returned to London by the beginning of the next week.",
        "We would have returned to London by the beginning of the next week.",
        "We will has returned to London by the beginning of the next week."
      ],
      [
        "The father will have finished his work by 9 o'clock.",
        "The father will finishing his work by 9 o'clock.",
        "The father will be finish his work by 9 o'clock."
      ],
      [
        "He will have opened his shop by the end of the month.",
        "He will have opens his shop by the end of the month.",
        "He would have opened his shop by the end of the month."
      ],
      /* - */ [
        "We won't have eaten this cake by the end of the celebration.",
        "We won't has eaten this cake by the end of the celebration.",
        "We won't eaten this cake by the end of the celebration."
      ],
      [
        "I won't have been a teacher for 2 years in a month.",
        "I won't have be a teacher for 2 years in a month.",
        "I won't be a teacher for 2 years in a month."
      ],
      [
        "He won't have spent all his money by the end of the month.",
        "He won't spend all his money by the end of the month.",
        "He won't spent all his money by the end of the month."
      ],
      [
        "Mark won't have learned his lesson by the end of the day.",
        "Mark won't learn his lesson by the end of the day.",
        "Mark won't have learn his lesson by the end of the day."
      ],
      /* ? */ [
        "Will you have finished your work before he comes?",
        "Would you have finished your work before he comes?",
        "Would you had finished your work before he comes?"
      ],
      [
        "Will your brother have arrived here by Tuesday?",
        "Will your brother has arrived here by Tuesday?",
        "Will your brother have arrive here by Tuesday?"
      ],
      [
        "Will she have left before the performance begins?",
        "Will she leaves before the performance begins?",
        "Will she has left before the performance begins?"
      ],
      [
        "Will the film have finished by 10 pm?",
        "Would the film have finish by 10 pm?",
        "Would the film has finished by 10 pm?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Он ремонтирует окно всё утро.",
          "Дети гуляют в лесу уже два часа.",
          "Я ищу тебя всё утро.",
          "Вода кипит уже 20 минут.",
          /* - */ "Я не работаю усердно всю неделю.",
          "Он не наблюдает за мной уже час.",
          "Она не обучает испанскому с прошлого года.",
          "Мы не ремонтируем машину уже месяц.",
          /* ? */ "Как долго вы сидите здесь?",
          "Этот мужчина читает газету уже два часа?",
          "Кто разрабатывает эту программу уже полгода?",
          "Ты уже долго ждёшь меня?",
        ], [
      /* + */ [
        "He has been repairing the window all morning.",
        "He had been repairing the window all morning.",
        "He have been repairing the window all morning."
      ],
      [
        "The kids have been walking in the woods for 2 hours.",
        "The kids have walking in the woods for 2 hours.",
        "The kids have be walking in the woods for 2 hours."
      ],
      [
        "I have been looking for you the whole morning.",
        "I has been looking for you the whole morning.",
        "I had been looking for you the whole morning."
      ],
      [
        "The water has been boiling for 20 minutes already.",
        "The water is been boiling for 20 minutes already.",
        "The water haves been boiling for 20 minutes already."
      ],
      /* - */ [
        "I haven't been working hard all week.",
        "I hadn't been working hard all week.",
        "I haven't been worked hard all week."
      ],
      [
        "He hasn't been observing me for an hour.",
        "He hasn't been observe me for an hour.",
        "He hasn't been observed me for an hour."
      ],
      [
        "She hasn't been teaching Spanish since last year.",
        "She havn't been teaching Spanish since last year.",
        "She havesn't been teaching Spanish since last year."
      ],
      [
        "We haven't been repairing the car for a month.",
        "We hadn't been repairing the car for a month.",
        "We haven't been repaired the car for a month."
      ],
      /* ? */ [
        "How long have you been sitting here?",
        "How long you been sitting here?",
        "How long was you been sitting here?"
      ],
      [
        "Has this man been reading the newspaper for 2 hours?",
        "Had this man been reading the newspaper for 2 hours?",
        "Has this man been read the newspaper for 2 hours?"
      ],
      [
        "Who has been developing this program for half a year already?",
        "Who has developing this program for half a year already?",
        "Who has been develop this program for half a year already?"
      ],
      [
        "Have you been waiting for me for a long time?",
        "Have you waiting for me for a long time?",
        "Have you be waiting for me for a long time?"
      ],
    ]),
// IN PROGRESS
    Tuple2(
        /* + */ [
          "Он работал официантом несколько лет, прежде чем переехать в Москву.",
          "Я читал документацию два часа, прежде чем пойти домой.",
          "Джон чувствовал себя усталым, так как катался на велосипеде с утра.",
          "Она убиралась два часа, и поэтому у неё не осталось времени отдохнуть.",
          /* - */ "Мы уже час не смотрели фильм, когда он пришёл.",
          "Они не спали уже два часа, когда приехали гости.",
          "Джон выглядел счастливым, так как не работал много.",
          "Я не ждал долго, прежде чем приехал автобус.",
          /* ? */ "Как долго она готовила, прежде чем пришли гости?",
          "Как долго ты смотрел телевизор, прежде чем решил пойти в душ?",
          "Как долго шёл дождь, когда ты ушёл из дома?",
          "Как долго Антонио рисовал, прежде чем продал свою первую картину?",
        ], [
      /* + */ [
        "He had been working for some years as a waiter before moving to Moscow.",
        "He has been working for some years as a waiter before moving to Moscow.",
        "He was been working for some years as a waiter before moving to Moscow."
      ],
      [
        "I had been reading the documentation before going home.",
        "I had be reading the documentation before going home.",
        "I had been read the documentation before going home."
      ],
      [
        "John felt tired as he had been riding the bike since morning.",
        "John felt tired as he had been rided the bike since morning.",
        "John felt tired as he had been rode the bike since morning."
      ],
      [
        "She had been cleaning the house so she didn't have time to relax.",
        "She was been cleaning the house so she didn't have time to relax.",
        "She was cleaned the house so she didn't have time to relax."
      ],
      /* - */ [
        "We hadn't been watching the film for an hour when he came.",
        "We hadn't been watched the film for an hour when he came.",
        "We haven't been watching the film for an hour when he came."
      ],
      [
        "They hadn't been sleeping for two hours when the guests came.",
        "They haven't been sleeping for two hours when the guests came.",
        "They hadn't been sleept for two hours when the guests came."
      ],
      [
        "John looked happy as he hadn't been working much.",
        "John looked happy as he hadn't been work much.",
        "John looked happy as he hadn't been worked much."
      ],
      [
        "I hadn't been waiting long before the bus arrived.",
        "I hadn't been waited long before the bus arrived.",
        "I hadn't be waiting long before the bus arrived."
      ],
      /* ? */ [
        "How long had she been cooking before the guests came?",
        "How long did she been cooking before the guests came?",
        "How long did she been cooked before the guests came?"
      ],
      [
        "How long had you been watching TV before you decided to have a shower?",
        "How long has you been watching TV before you decided to have a shower?",
        "How long have you been watching TV before you decided to have a shower?"
      ],
      [
        "How long had it been raining when you left your house?",
        "How long had it did raining when you left your house?",
        "How long had it be raining when you left your house?"
      ],
      [
        "How long had Antonio been painting before he sold his first picture?",
        "How long had Antonio be painting before he sold his first picture?",
        "How long was Antonio been painting before he sold his first picture?"
      ],
    ]),
// IN PROGRESS
    Tuple2(
        /* + */ [
          "Я буду лететь более 5 часов до Чикаго.",
          "Они будут жить 10 лет в Калифорнии к началу следующего года.",
          "Анвар будет работать в Америке уже 3 года к концу месяца.",
          "Мы будем гулять уже 5 часов к концу дня.",
          /* - */ "Она не будет работать здесь и двух лет к началу следующего месяца.",
          "Мэри не проработает достаточно долго к апрелю, чтобы пойти в отпуск.",
          "Я не проучусь на курсах французского достаточно долго к лету, чтобы поехать во Францию.",
          "В мае ещё не исполнится 3 года как я живу здесь.",
          /* ? */ "Как долго ты будешь работать здесь к началу 2022 года?",
          "Как долго он будет водить машину, когда ему исполнится 20?",
          "Сколько вы проработаете на фабрике к тому моменту, как уйдете на пенсию?",
          "Сколько лет художник будет писать свою картину, когда наконец закончит её?",
        ], [
      /* + */ [
        "I will have been flying over 5 hours when I get to Chicago.",
        "I will have flying over 5 hours when I get to Chicago.",
        "I will have be flying over 5 hours when I get to Chicago."
      ],
      [
        "They will have been living in California for 10 years by the beginning of the next year.",
        "They will have been live in California for 10 years by the beginning of the next year.",
        "They will have been lived in California for 10 years by the beginning of the next year."
      ],
      [
        "Anwar will have been working in the USA for 3 years by the end of the month.",
        "Anwar will has been working in the USA for 3 years by the end of the month.",
        "Anwar will has been work in the USA for 3 years by the end of the month."
      ],
      [
        "We will have been walking for 5 hours by the end of the day.",
        "We will had been walking for 5 hours by the end of the day.",
        "We will has been walking for 5 hours by the end of the day."
      ],
      /* - */ [
        "She won't have been working here for 2 years by the beginning of the next month.",
        "She willn't have been working here for 2 years by the beginning of the next month.",
        "She willn't have be working here for 2 years by the beginning of the next month."
      ],
      [
        "Mary won't have been working long enough by April to have a vacation.",
        "Mary won't had been working long enough by April to have a vacation.",
        "Mary won't has been working long enough by April to have a vacation."
      ],
      [
        "I won't have been taking French language course long enough by summer to go to France.",
        "I won't have been taken French language course long enough by summer to go to France.",
        "I won't have been took French language course long enough by summer to go to France."
      ],
      [
        "I won't have been living here for 3 years by May.",
        "I won't have being living here for 3 years by May.",
        "I won't have be living here for 3 years by May."
      ],
      /* ? */ [
        "How long will you have been working here by the beginning of 2022?",
        "How long would you have been working here by the beginning of 2022?",
        "How long will you had been working here by the beginning of 2022?"
      ],
      [
        "How long will he have been driving a car when he becomes 20",
        "How long will he been driving a car when he becomes 20?",
        "How long would he been driving a car when he becomes 20?"
      ],
      [
        "How long will you have been working at the factory when you retire?",
        "How long will you has been working at the factory when you retire?",
        "How long you have been working at the factory when you retire?"
      ],
      [
        "How many years will the painter have been painting his picture when he finishes it at last?",
        "How many years will the painter have been painted his picture when he finishes it at last?",
        "How many years will the painter have been paints his picture when he finishes it at last?"
      ],
    ]),

// DONE!
    Tuple2(
        /* + */ [
          "Комнату убирают каждый день.",
          "Ему подарили котёнка.",
          "Эту книгу открывают раз в год.",
          "Война будет прекращена.",
          /* - */ "Улицы не подметают тщательно.",
          "Они не были приглашены на представление.",
          "Ей не предложили чашку кофе.",
          "Дом не будет снесён.",
          /* ? */ "Его часто спрашивают?",
          "Когда был основан этот город?",
          "Где будет построено здание?",
          "Когда этот проект будет закончен?",
        ], [
      /* + */ [
        "The room is cleaned every day.",
        "The room cleaned every day.",
        "The room be cleaned every day."
      ],
      ["He was given a kitten.", "He was gave a kitten.", "He given a kitten."],
      [
        "This book is opened once a year.",
        "This book be opened once a year.",
        "This book is open once a year."
      ],
      [
        "The war will be ended.",
        "The war will be end.",
        "The war is be ended."
      ],
      /* - */ [
        "The streets aren't swept properly.",
        "The streets don't swept properly.",
        "The streets isn't swept properly."
      ],
      [
        "They weren't invited to the performance.",
        "They didn't invited to the performance.",
        "They wasn't invited to the performance."
      ],
      [
        "She wasn't offered a cup of tea.",
        "She wasn't offer a cup of tea.",
        "She didn't offer a cup of tea."
      ],
      [
        "The house won't be demolished.",
        "The house won't been demolished.",
        "The house willn't be demolished."
      ],
      /* ? */ [
        "Is he asked often?",
        "Are he asked often?",
        "Am he asked often?"
      ],
      [
        "When was this town founded?",
        "When did this town founded?",
        "When was this town found?"
      ],
      [
        "Where will the building be built?",
        "Where would the building be built?",
        "Where will the building built?"
      ],
      [
        "When will this project be finished?",
        "When is this project be finished?",
        "When does this project be finished?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Дом сейчас сносят.",
          "Автобус красят в данный момент.",
          "Крышу ремонтировали с 5 до 7 часов вечера.",
          "Сервер чинили вчера весь день.",
          /* - */ "Котят не кормят в данный момент.",
          "Книгу не читают сейчас.",
          "Проект не готовили с понедельника по пятницу.",
          "Комнату не убирали пока он ждал.",
          /* ? */ "Программа выполняется сейчас?",
          "Доклад читают в данный момент?",
          "Ворота красили всё утро?",
          "Газон стригли с 7 до 8 утра?",
        ], [
      /* + */ [
        "The house is being demolished.",
        "The house is been demolished.",
        "The house are being demolished."
      ],
      [
        "The bus is being painted at the moment.",
        "The bus is painted at the moment.",
        "The bus was being painted at the moment."
      ],
      [
        "The roof was being repaired from 5 till 7 pm.",
        "The roof was being repair from 5 till 7 pm.",
        "The roof was being repairing from 5 till 7 pm."
      ],
      [
        "The server was being fixed the whole day yesterday.",
        "The server was being fix the whole day yesterday.",
        "The server was been fixed the whole day yesterday."
      ],
      /* - */ [
        "The kittens aren't being fed now.",
        "The kittens aren't being feed now.",
        "The kittens aren't being feeded now."
      ],
      [
        "The book isn't being read now.",
        "The book not being read now.",
        "The book aren't being read now."
      ],
      [
        "The project wasn't being prepared form Monday till Friday.",
        "The project will not being prepared form Monday till Friday.",
        "The project doesn't being prepared form Monday till Friday."
      ],
      [
        "The room wasn't being cleaned while he was waiting.",
        "The room weren't being cleaned while he was waiting.",
        "The room isn't being cleaned while he was waiting."
      ],
      /* ? */ [
        "Is the program being executed now?",
        "Are the program being executed now?",
        "Am the program being executed now?"
      ],
      [
        "Is the report being read at the moment?",
        "Does the report being read at the moment?",
        "Do the report being read at the moment?"
      ],
      [
        "Were the gates being painted the whole morning?",
        "Were the gates been painted the whole morning?",
        "Were the gates be painted the whole morning?"
      ],
      [
        "Was the lawn being cut from 7 till 8 am?",
        "Was the lawn being cutted from 7 till 8 am?",
        "Was the lawn be cut from 7 till 8 am?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Еда только что была куплена.",
          "Мы уже были приглашены.",
          "Ваза была разбита, прежде чем приехали родители.",
          "Работа будет окончена к началу следующей недели.",
          /* - */ "Коровы еще не накормлены.",
          "Пирог не был испечён, прежде чем пришли гости.",
          "Картина не была написана к 2 часам дня вчера.",
          "Здание не будет достроено к началу следующего года.",
          /* ? */ "Тебя уже спросили?",
          "Их поздравили, прежде чем они вернулись домой?",
          "Обед был готов, когда вы приехали к ним?",
          "Будет ли эссе написано к 7 часам вечера?",
        ], [
      /* + */ [
        "The food has just been bought.",
        "The food have just been bought.",
        "The food has just been buyed."
      ],
      [
        "We have already been invited.",
        "We have already be invited.",
        "We have already been invite."
      ],
      [
        "The vase had been broken before parents came.",
        "The vase been broken before parents came.",
        "The vase was been broken before parents came."
      ],
      [
        "The work will have been done by the beginning of the next week.",
        "The work will have done by the beginning of the next week.",
        "The work will done by the beginning of the next week."
      ],
      /* - */ [
        "The cows haven't been fed yet.",
        "The cows hasn't been fed yet.",
        "The cows haven't been feed yet."
      ],
      [
        "The pie hadn't been baked before the guests came.",
        "The pie didn't been baked before the guests came.",
        "The pie hadn't been bakeed before the guests came."
      ],
      [
        "The picture hadn't been painted by 2 pm yesterday.",
        "The picture wasn't been painted by 2 pm yesterday.",
        "The picture hadn't been painting by 2 pm yesterday."
      ],
      [
        "The building won't have been built by the beginning of the next year.",
        "The building won't built by the beginning of the next year.",
        "The building willn't have been built by the beginning of the next year."
      ],
      /* ? */ [
        "Have you been asked already?",
        "Was you been asked already?",
        "Have you be asked already?"
      ],
      [
        "Had they been congratulated before they had come home?",
        "Had they been congratulating before they had come home?",
        "Has they been congratulated before they had come home?"
      ],
      [
        "Had the dinner been cooked when you came to their place?",
        "Had the dinner did cooked when you came to their place?",
        "Was the dinner be cooked when you came to their place?"
      ],
      [
        "Will the essay have been written by 7 pm?",
        "Will the essay been written by 7 pm?",
        "Will the essay has been written by 7 pm?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Он боялся, что опоздает.",
          "Я надеялся, что им понравится фильм.",
          "Том заявил, что они будут работать там с двух до трёх.",
          "Она сказала, что мы закончим проект до конца следующей недели.",
          /* - */ "Люси сообщила нам, что не придёт вовремя.",
          "Она сказала мне, что она не будет гулять в это время на следующий день.",
          "Он знал, что не закончит убираться в комнате к 6 часам вечера.",
          "Мне сказали, что проблема не будет решена к концу дня.",
          /* ? */ "Он спросил, придёте ли вы?",
          "Учитель спросил вас, сделаете ли вы домашнюю работу?",
          "Вы выяснили, будут ли они всё ещё играть в это время?",
          "Вы спросили, вернётся ли он к 2 часам ночи?",
        ], [
      /* + */ [
        "He was afraid he would be late.",
        "He was afraid he would been late.",
        "He was afraid he will be late."
      ],
      [
        "I hoped that they would like the film.",
        "I hoped that they would be like the film.",
        "I hoped that they would liked the film."
      ],
      [
        "Tom announced that they would be working there from 2 till 3.",
        "Tom announced that they would working there from 2 till 3.",
        "Tom announced that they would have working there from 2 till 3."
      ],
      [
        "She said that we would have finished the project by the end of the next week.",
        "She said that we would did finished the project by the end of the next week.",
        "She said that we will have finished the project by the end of the next week."
      ],
      /* - */ [
        "Lucy told us that she wouldn't come on time.",
        "Lucy told us that she won't come on time.",
        "Lucy told us that she wouldn't coming on time."
      ],
      [
        "She told me that she wouldn't be walking at that time next day.",
        "She told me that she wouldn't be walk at that time next day.",
        "She told me that she wouldn't walking at that time next day."
      ],
      [
        "He knew that he wouldn't have finished cleaning the room by 6 pm.",
        "He knew that he wouldn't had finished cleaning the room by 6 pm.",
        "He knew that he wouldn't has finished cleaning the room by 6 pm."
      ],
      [
        "I was told that the problem wouldn't have been solved by the end of the day.",
        "I was told that the problem would not have did solved by the end of the day.",
        "I was told that the problem wouldn't have be solved by the end of the day."
      ],
      /* ? */ [
        "Did he ask if you would come?",
        "Did he ask if you will come?",
        "Did he ask if you would coming?"
      ],
      [
        "Did the teacher ask you if you would do the homework?",
        "Did the teacher ask you if you would done the homework?",
        "Did the teacher ask you if you will do the homework?"
      ],
      [
        "Did you inquire if they would still be playing at that time?",
        "Did you inquire if they would still do playing at that time?",
        "Did you inquire if they would still be played at that time?"
      ],
      [
        "Did you ask if he would have come back by 2 am?",
        "Did you ask if he would has come back by 2 am?",
        "Did you ask if he would had come back by 2 am?"
      ],
    ]),
// DONE!
    Tuple2(
        /* + */ [
          "Я не знал, где он живёт.",
          "Он сказал, что Джейн спит.",
          "Джон сообщил нам, что он вернулся домой.",
          "Учитель сказал, что проверил домашнюю работу на прошлой неделе.",
          /* - */ "Она не знала, что он не работает.",
          "Мальчик сказал, что его друзья не играют в футбол в данный момент.",
          "Его друг сообщил мне, что Марк не вернулся из Лондона.",
          "Я сказал, что мы не ездили на море в том году.",
          /* ? */ "Он спросил, учишься ли ты там?",
          "Она выяснила, купил ли он машину в прошлом месяце?",
          "Элис узнала, ехал ли он домой в тот момент?",
          "Они спросили, посетили ли мы музей?",
        ], [
      /* + */ [
        "I didn't know where he lived.",
        "I don't know where he lived.",
        "I doesn't know where he lived."
      ],
      [
        "He said that Jane was sleeping.",
        "He said that Jane does sleeping.",
        "He said that Jane is sleeping."
      ],
      [
        "John told us that he had come home then.",
        "John told us that he has come home then.",
        "John told us that he come home then."
      ],
      [
        "The teacher said that he had checked homework the previous week.",
        "The teacher said that he have checked homework the previous week.",
        "The teacher said that he had check homework the previous week."
      ],
      /* - */ [
        "She didn't know that he didn't work.",
        "She didn't know that he wasn't work.",
        "She didn't know that he doesn't work."
      ],
      [
        "The boy said that his friends weren't playing soccer that moment.",
        "The boy said that his friends didn't playing soccer that moment.",
        "The boy said that his friends weren't played soccer that moment."
      ],
      [
        "His friend told me that Mark hadn't returned from London.",
        "His friend told me that Mark hadn't return from London.",
        "His friend told me that Mark hasn't returned from London."
      ],
      [
        "I said that we hadn't gone to the sea that year.",
        "I said that we hadn't went to the sea that year.",
        "I said that we hadn't go to the sea that year."
      ],
      /* ? */ [
        "Did he ask if you learned there?",
        "Did he ask if you learn there?",
        "Did he ask if you learning there?"
      ],
      [
        "Did she inquire if he had bought a car the previous month?",
        "Did she inquired if he did bought a car the previous month?",
        "Did she inquire if he bought a car the previous month?"
      ],
      [
        "Did Alice find out whether he was driving home that moment?",
        "Did Alice find out whether he was drove home that moment?",
        "Did Alice find out whether he drove home that moment?"
      ],
      [
        "Did they ask if we had visited the museum?",
        "Did they ask if we have visited the museum?",
        "Did they ask if they did visited the museum?"
      ],
    ]),
  ];
}
