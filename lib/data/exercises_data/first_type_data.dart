import 'package:tense/data/tenses_data.dart';
import 'package:tuple/tuple.dart';

abstract class FirstTypeData {
  static Map<String, Tuple2<List<String>, List<List<String>>>> dataAsMap() {
    Map<String, Tuple2<List<String>, List<List<String>>>> output = {};
    List<String> cutArray = TensesData.tensesAsArray();
    cutArray.removeAt(0);
    cutArray.asMap().forEach((index, element) {
      output[element] = data[index];
    });
    return output;
  }

  static const List<Tuple2<List<String>, List<List<String>>>> data = [
    Tuple2(
        /* + */ [
          "He ___ a doctor.",
          "We ___ students.",
          "John ___ a good worker in the past.",
          "They ___ here yesterday.",
          "Joe _____ in Moscow tomorrow.",
          /* - */ "I _____ good at sport.",
          "This boy ___ my friend.",
          "We ___ at home yesterday.",
          "Jessica ___ an actress.",
          "He _____ in time.",
          /* ? */ "___ your brother a policeman?",
          "___ this man our new teacher?",
          "___ you in Russia in 1992?",
          "Where ___ he in Moscow last year?",
          "___ he ___ at work tomorrow?"
        ], [
      /* + */ ["is", "are", "am", "be"],
      ["are", "is", "am", "be"],
      ["was", "were", "are", "be"],
      ["were", "was", "been", "are"],
      ["will be", "will", "is", "be"],
      /* - */ ["am not", "don't", "isn't", "aren't"],
      ["isn't", "not", "aren't", "am not"],
      ["weren't", "wasn't", "did not", "have not"],
      ["wasn't", "weren't", "were not", "did not"],
      ["won't be", "willn't be", "don't be", "not"],
      /* ? */ ["Is", "Are", "Am", "Be"],
      ["Is", "Are", "Am", "Does"],
      ["Were", "Was", "Is", "Are"],
      ["was", "were", "did", "be"],
      ["Will ... be", "Is ... be", "Will ... is", "Will ... do"]
    ]),
    Tuple2(
        /* + */ [
          "Michael ___ to school every day.",
          "Sara ___ chocolate.",
          "We ___ English very well.",
          "I ___ books every day.",
          "John often ___ in the park.",
          /* - */ "Sam _____ at the shop. He works at school.",
          "My sister _____ to have a cat. She likes dogs.",
          "I _____ an expensive car. My car is cheap.",
          "My father _____ coffee. He prefers tea.",
          "She _____ much, but she is plump.",
          /* ? */ "___ you ___ the way?",
          "When ___ he ___ to bed?",
          "Why ___ Tom ___ home so late?",
          "___ they ___ their grandparents every month?",
          "How much ___ this phone ___?"
        ], [
      /* + */ ["goes", "go", "went", "going"],
      ["likes", "like", "liked", "liking"],
      ["speak", "spoke", "speaks", "speaking"],
      ["read", "reading", "reads", "readed"],
      ["walks", "walk", "walking", "walked"],
      /* - */ ["doesn't work", "don't work", "not work", "doesn't works"],
      ["doesn't want", "don't want", "not want", "doesn't wants"],
      ["don't have", "doesn't have", "not have", "doesn't has"],
      ["doesn't like", "don't like", "liken't", "doesn't likes"],
      ["doesn't eat", "don't eat", "not eat", "don't eats"],
      /* ? */ [
        "Do ... know",
        "Does ... know",
        "Do ... knowing",
        "Does ... knows"
      ],
      ["does ... go", "do ... goes", "has ... go", "does ... goes"],
      ["does ... come", "do ... comes", "have ... comes", "do ... come"],
      ["Do ... visit", "Does ... visit", "Have ... visit", "Do ... visits"],
      ["does ... cost", "does ... costs", "do ... cost", "have ... costs"]
    ]),
    Tuple2(
        /* + */ [
          "Tom ___ football with his friends yesterday.",
          "They ___ to Moscow last year.",
          "The company ___ a new building several days ago.",
          "I ___ home at 8:30 pm.",
          "She ___ Spanish when she was young.",
          /* - */ "We _____ for a walk because it was cold.",
          "Sarah _____ the cake two hours ago.",
          "My friends _____ to see me yesterday.",
          "Mary _____ her ill sister because she was busy.",
          "He _____ to Moscow last week.",
          /* ? */ "___ you ___ this car last month?",
          "What ___ he ___ you for your birthday last year?",
          "How ___ she ___ into the building yesterday?",
          "Why ___ Tom ___ this house?",
          "___ she ___ to see you two days ago?",
        ], [
      /* + */ ["played", "play", "plays", "playing"],
      ["went", "go", "gone", "going"],
      ["built", "builded", "build", "building"],
      ["came", "come", "comed", "coming"],
      ["learned", "learn", "learnen", "learning"],
      /* - */ ["didn't go", "didn't went", "don't go", "don't went"],
      ["didn't bake", "didn't baked", "don't bake", "don't baked"],
      ["didn't come", "didn't came", "don't came", "don't comed"],
      ["didn't visit", "didn't visited", "don't visited", "don't visit"],
      ["didn't fly", "didn't flew", "don't flied", "don't flew"],
      /* ? */ [
        "Did ... buy",
        "Does ... bought",
        "Did ... buyed",
        "Did ... bought"
      ],
      ["did ... give", "does ... given", "do ... gave", "have ... give"],
      ["did ... get", "did ... got", "do ... got", "does ... got"],
      ["did ... choose", "did ... chosed", "did ... chosen", "does ... chosed"],
      ["Did ... come", "Does ... came", "Do ... came", "Have ... come"]
    ]),
    Tuple2(
        /* + */ [
          "My friends ___ to my place next week.",
          "Tomorrow we ___ a new house.",
          "She ___ her parents next month.",
          "One day I ___ famous.",
          "He ___ me the story tomorrow.",
          /* - */ "I _____ my homework. I have no time.",
          "My parents _____ me a new smartphone.",
          "Nick _____ basketball tomorrow because he is ill.",
          "He _____ the exercises. He is tired.",
          "Jessica _____ to New York next month because she has no money.",
          /* ? */ "Where ___ you ___ tomorrow morning?",
          "___ he ___ me when he comes to the town?",
          "What ___ she ___ next week?",
          "___ Mr. Brown ___ his vacation at sea?",
          "How ___ they ___ here? By bus or by taxi?",
        ], [
      /* + */ ["will come", "come", "coming", "came"],
      ["will buy", "buy", "will bought", "buys"],
      ["will visit", "visited", "will visited", "visit"],
      ["will become", "become", "becomes", "becoming"],
      ["will tell", "told", "tells", "will told"],
      /* - */ ["won't do", "don't done", "don't do", "won't doing"],
      ["won't buy", "aren't buy", "isn't buy", "won't buying"],
      ["won't play", "will not playing", "isn't played", "don't play"],
      ["won't do", "are not doing", "won't done", "didn't do"],
      ["won't fly", "will not flew", "won't flied", "won't flying"],
      /* ? */ ["will ... be", "will be ... go", "is ... been", "is ... being"],
      [
        "Will ... visit",
        "Will ... visited",
        "Does ... visits",
        "Will ... visiting"
      ],
      ["will ... do", "are ... do", "are ... did", "is ... doing"],
      [
        "Will ... spend",
        "Is ... spending",
        "Will ... be spent",
        "Will ... spends"
      ],
      ["will ... get", "do ... got", "will ... gotted", "does ... getting"],
    ]),
    Tuple2(
        /* + */ [
          "John _____ his car at the moment.",
          "We _____ on the sofa and ___ TV.",
          "She _____ with her dog in the yard.",
          "I _____ to fix my computer now.",
          "Wendy _____ a very interesting story at the moment.",
          /* - */ "The children _____ right now.",
          "My brother _____ home now.",
          "We _____ the walls at the moment.",
          "It _____. The weather is sunny.",
          "Samantha _____ to music now. She doesn't like it.",
          /* ? */ "Where ___ they ___? Do you know?",
          "___ he ___ the film now? No, he is sleeping.",
          "Why ___ you ___ ?",
          "___ Mrs. Smith ___ in the park with her husband?",
          "What ___ she ___? I can't realize.",
        ], [
      /* + */ ["is washing", "is wash", "washes", "are washing"],
      [
        "are sitting ... watching",
        "are sit ... watch",
        "is sitting ... watching",
        "is sit ... watch"
      ],
      ["is playing", "are playing", "is play", "is plays"],
      ["am trying", "try", "tries", "is trying"],
      ["is telling", "was telling", "are telling", "tell"],
      /* - */ [
        "aren't sleeping",
        "isn't sleeping",
        "don't sleep",
        "doesn't sleep"
      ],
      ["isn't driving", "aren't driving", "isn't drives", "won't drive"],
      ["aren't painting", "is not painting", "isn't paint", "aren't paint"],
      ["isn't raining", "are not raining", "won't raining", "didn't raining"],
      [
        "isn't listening",
        "aren't listening",
        "won't listening",
        "isn't listen"
      ],
      /* ? */ ["are ... going", "is ... going", "be ... going", "are ... go"],
      [
        "Is ... watching",
        "Are ... watching",
        "Does ... watching",
        "Is ... watch"
      ],
      ["are ... running", "are ... run", "is ... running", "do ... running"],
      ["Is ... walking", "Is ... walk", "Are ... walk", "Does ... walikng"],
      ["is ... doing", "are ... doing", "are ... does", "is ... do"],
    ]),
    Tuple2(
        /* + */ [
          "Mary _____ the house all morning.",
          "They _____ home at 5 o'clock yesterday.",
          "She _____ a book from 7 am till 9 am.",
          "Lucy _____ a cake when her father came.",
          "We _____ our room when the telephone rang.",
          /* - */ "The kittens _____ on the sofa when I came.",
          "He _____ to school at this time yesterday. He was in bed.",
          "The children _____ computer games all day.",
          "Bob _____ an ice cream when he met his friend.",
          "They _____ TV all evening. They had no time.",
          /* ? */ "___ he ___ the book when you came into the room?",
          "___ they ___ to the cinema at 7 pm? No, they were already there.",
          "What ___ you ___ on the piano?",
          "___ she ___ her homework from 6 pm till 9 pm yesterday?",
          "What ___ Mike ___ on the paper?",
        ], [
      /* + */ ["was cleaning", "is cleaning", "cleaned", "were cleaning"],
      ["were going", "were go", "is going", "was going"],
      ["was reading", "were reading", "read", "is reading"],
      ["was making", "were making", "made", "is making"],
      ["were cleaning", "was cleaning", "are cleaning", "cleaned"],
      /* - */ [
        "weren't sleeping",
        "isn't sleeping",
        "didn't sleep",
        "wasn't sleeping"
      ],
      ["wasn't going", "isn't going", "weren't go", "weren't going"],
      ["weren't playing", "aren't playing", "wasn't playing", "aren't play"],
      ["wasn't eating", "are not eating", "isn't eating", "weren't eating"],
      ["weren't watching", "aren't watching", "isn't watching", "didn't watch"],
      /* ? */ [
        "Was ... reading",
        "Is ... reading",
        "Did ... read",
        "Were ... reading"
      ],
      ["Were ... going", "Are ... going", "Did ... go", "Was ... going"],
      [
        "were ... playing",
        "are ... playing",
        "was ... playing",
        "did ... play"
      ],
      ["Was ... doing", "Is ... doing", "Are ... doing", "Were ... doing"],
      ["was ... drawing", "is ... drawing", "are ... drawing", "did ... draw"],
    ]),
    Tuple2(
        /* + */ [
          "We _____ our house the whole day tomorrow.",
          "Sam _____ to his office this time tomorrow.",
          "Rachel _____ in her bed at 9 o'clock tonight.",
          "Steve _____ a breakfast this time on Sunday.",
          "They _____ near the school from 10 am till 3 pm tomorrow.",
          /* - */ "The children _____ their exams at 4 pm tomorrow.",
          "Betsey _____ in the park with her boyfriend at 7 pm tonight. She is ill.",
          "The boys _____ a marathon this time on Friday.",
          "She _____ a letter to her husband at 5 o'clock tomorrow evening.",
          "They _____ football from 3 pm till 5 pm today.",
          /* ? */ "___ Samantha _____ in the park at 7 am tomorrow?",
          "___ they _____ home this time tomorrow? No, they will be at work.",
          "At what time ___ you _____ dinner at the restaurant tonight?",
          "Where ___ we _____ basketball at 11 am on Monday?",
          "What ___ John _____ this time tomorrow?",
        ], [
      /* + */ [
        "will be cleaning",
        "are be cleaning",
        "will cleaning",
        "will be clean"
      ],
      ["will be driving", "will be drive", "is driving", "would drive"],
      ["will be sleeping", "were sleeping", "will sleeping", "would sleeping"],
      ["will be cooking", "will cooking", "cooks", "is cooking"],
      ["will be working", "was working", "will working", "were working"],
      /* - */ [
        "won't be taking",
        "isn't taking",
        "aren't taking",
        "won't taken"
      ],
      [
        "won't be jogging",
        "aren't jogging",
        "isn't jogging",
        "will not jogging"
      ],
      [
        "won't be running",
        "don't be running",
        "aren't running",
        "isn't running"
      ],
      [
        "won't be writing",
        "is not writing",
        "aren't writing",
        "will be not writing"
      ],
      ["won't be playing", "aren't playing", "isn't playing", "don't play"],
      /* ? */ [
        "Will ... be walking",
        "Will ... walking",
        "Will ... be walk",
        "Would ... walk"
      ],
      [
        "Will ... be driving",
        "Will ... drive",
        "Will ... be drive",
        "Is ... driving"
      ],
      [
        "will ... be having",
        "will ... be have",
        "will ... having",
        "are ... be having"
      ],
      [
        "will ... be playing",
        "is ... be playing",
        "will ... playing",
        "will ... play"
      ],
      [
        "will ... be doing",
        "will ... doing",
        "will ... done",
        "will ... be do"
      ],
    ]),
    Tuple2(
        /* + */ [
          "I ___ already ___ my homework.",
          "Sue ___ just ___ home with her friends.",
          "Joe ___ recently ___ a nice new car.",
          "They _____ the race.",
          "We _____ our keys and can't enter the house.",
          /* - */ "Mike _____ his work yet.",
          "Doris _____ the TV off.",
          "The girls _____ home from school yet.",
          "She _____ an e-mail from her father.",
          "You _____ this amazing film yet!",
          /* ? */ "___ John ___ his new bestseller?",
          "When ___ you ___ him?",
          "___ they ___ to Moscow today?",
          "Where ___ he ___?",
          "What ___ I ___?",
        ], [
      /* + */ ["have ... done", "has ... done", "has ... did", "have ... did"],
      ["has ... come", "have ... come", "has ... came", "has ... comed"],
      ["has ... bought", "have ... buy", "has ... buy", "has ... buyed"],
      ["have finished", "has finished", "has finish", "have finish"],
      ["have lost", "has lost", "have lose", "has lose"],
      /* - */ ["hasn't done", "haven't done", "hasn't did", "haven't did"],
      [
        "hasn't switched",
        "haven't switched",
        "hasn't switch",
        "haven't switch"
      ],
      ["haven't come", "didn't come", "hasn't come", "haven't comed"],
      ["hasn't got", "haven't got", "hasn't get", "hasn't getted"],
      ["haven't seen", "haven't see", "haven't saw", "hasn't seen"],
      /* ? */ [
        "Has ... read",
        "Has ... readed",
        "Have ... read",
        "Did ... read"
      ],
      ["have ... met", "has ... meet", "have ... meeted", "has ... met"],
      ["Have ... been", "Have ... was", "Has ... be", "Has ... been"],
      ["has ... gone", "has ... go", "has ... went", "have ... gone"],
      ["have ... done", "have ... did", "has ... done", "have ... do"],
    ]),
    Tuple2(
        /* + */ [
          "I _____ all the work before going home.",
          "Dave _____ the room by 5 o'clock.",
          "They _____ their grandfather before he went to the countryside.",
          "We _____ back by 7 pm yesterday.",
          "Bob _____ his present before her birthday.",
          /* - */ "You _____ all exercises before the lesson finished.",
          "He _____ his juice by the end of the party.",
          "The children _____ to the camp by the end of the day.",
          "I _____ him before he got here.",
          "The boy _____ his teeth before going to bed.",
          /* ? */ "___ he ___ before you came?",
          "Where ___ Nick ___ before he went to our place?",
          "___ you ever ___ the book before passing that exam yesterday?",
          "Where ___ they ___ before they moved to Moscow?",
          "Why ___ Sam ___ the party before it finished?",
        ], [
      /* + */ ["had finished", "has finished", "have finished", "have finish"],
      ["had painted", "have painted", "has painted", "has paint"],
      ["had visited", "will have visited", "has visited", "have visited"],
      ["had come", "has come", "have come", "have came"],
      ["had given", "has gave", "have given", "have gave"],
      /* - */ ["hadn't done", "haven't done", "hasn't did", "hasn't done"],
      ["hadn't drunk", "haven't drunk", "hasn't drunk", "haven't drink"],
      [
        "hadn't returned",
        "didn't return",
        "hasn't returned",
        "haven't returned"
      ],
      ["hadn't called", "haven't called", "hasn't called", "hadn't cold"],
      ["hadn't brushed", "haven't brush", "haven't brushed", "hasn't brushed"],
      /* ? */ [
        "Had ... left",
        "Has ... leave",
        "Have ... leave",
        "Had ... leave"
      ],
      ["had ... been", "has ... be", "have ... was", "has ... been"],
      ["Had ... opened", "Have ... opened", "Has ... open", "Has ... opened"],
      ["had ... lived", "has ... live", "has ... living", "have ... lived"],
      ["had ... left", "have ... left", "has ... leave", "have ... leave"],
    ]),
    Tuple2(
        /* + */ [
          "He _____ all his meal before his mother comes.",
          "She _____ the letter by tomorrow morning.",
          "The sportsmen _____ the cup by the end of the month.",
          "Sarah _____ back by 9 pm tomorrow.",
          "I _____ the drawing by Tuesday evening.",
          /* - */ "My grandma _____ the pie by 3 pm.",
          "He _____ his car by the end of the day.",
          "Sam _____ the problem before they come and see it.",
          "The workers _____ the house by the end of the month.",
          "My brother _____ all his homework before the father returns home.",
          /* ? */ "___ you _____ your school project by Monday?",
          "How ___ he _____ all these hamburgers by the end of the hour?",
          "___ Susan _____ such amount of money by the end of the summer?",
          "___ the boy _____ the broken mirror before his parents come?",
          "___ the mother _____ the dinner before we come?",
        ], [
      /* + */ [
        "will have eaten",
        "will had eaten",
        "will has eaten",
        "would have eaten"
      ],
      ["will have sent", "will had send", "will have send", "will had sent"],
      ["will have won", "will have win", "will has won", "would have won"],
      ["will have come", "will has come", "will had come", "will have came"],
      [
        "will have finished",
        "will had finished",
        "will has finished",
        "would have finished"
      ],
      /* - */ [
        "won't have baked",
        "won't have bake",
        "won't have bade",
        "won't had baked"
      ],
      [
        "won't have fixed",
        "won't had fixed",
        "won't have fix",
        "won't had fix"
      ],
      [
        "won't have solved",
        "won't had solved",
        "won't has solve",
        "won't have solve"
      ],
      [
        "won't have built",
        "won't have builded",
        "won't has built",
        "won't have builded"
      ],
      ["won't have done", "won't have did", "won't have do", "won't had done"],
      /* ? */ [
        "Will ... have finished",
        "Will ... had finished",
        "Will ... have finish",
        "Will ... had finish"
      ],
      [
        "will ... have eaten",
        "will ... had eaten",
        "would ... have ate",
        "will ... have eat"
      ],
      [
        "Will ... have saved",
        "Will ... has saved",
        "Will ... had saved",
        "Will ... have save"
      ],
      [
        "Will ... have hidden",
        "Will ... have hide",
        "Will ... had hidden",
        "Will ... have hidded"
      ],
      [
        "Will ... have cooked",
        "Will ... have cook",
        "Will ... had cooked",
        "Will ... have cooking"
      ],
    ]),
    Tuple2(
        /* + */ [
          "We _____ for you for 2 hours.",
          "They _____ chess since 11 am.",
          "The painter _____ his masterpiece for 3 years.",
          "I _____ for my exam since evening.",
          "You _____ computer games for 4 hours already!",
          /* - */ "He _____ the dishes since the end of the party.",
          "Mike _____ in the garden since noon.",
          "I _____ at this man since he entered the hall.",
          "The teacher _____ the copybooks since the lesson finished.",
          "My father _____ since 5 am. He has much to do.",
          /* ? */ "___ they _____ the fence since early morning?",
          "Why ___ he _____ at the computer for 3 hours already?",
          "Where ___ Lisa _____ for 5 hours?",
          "___ you _____?",
          "How long ___ I _____?",
        ], [
      /* + */ [
        "have been waiting",
        "has been waiting",
        "has waiting",
        "had waiting"
      ],
      [
        "have been playing",
        "has been playing",
        "have playing",
        "haves been playing"
      ],
      [
        "has been creating",
        "have been creating",
        "have created",
        "has created"
      ],
      [
        "have been preparing",
        "had been preparing",
        "has been preparing",
        "have been prepared"
      ],
      [
        "have been playing",
        "has been playing",
        "have been play",
        "had been playing"
      ],
      /* - */ [
        "hasn't been washing",
        "hadn't been washing",
        "haven't been washing",
        "hasn't been washed"
      ],
      [
        "hasn't been working",
        "haven't been working",
        "hadn't been working",
        "hasn't been worked"
      ],
      [
        "haven't been looking",
        "hasn't been looking",
        "hadn't been looking",
        "haven't looking"
      ],
      [
        "hasn't been checking",
        "hadn't been checking",
        "hasn't been checked",
        "hadn't been checked"
      ],
      [
        "hasn't been sleeping",
        "hadn't  sleeping",
        "hasn't sleeping",
        "hadn't been sleeping"
      ],
      /* ? */ [
        "Have ... been painting",
        "Had ... been painting",
        "Has ... been painting",
        "Have ... been painted"
      ],
      [
        "has ... been sitting",
        "have ... been sitting",
        "had ... been sitting",
        "has ... been sitted"
      ],
      [
        "has ... been walking",
        "had ... been walking",
        "have ... been walking",
        "have ... been walked"
      ],
      [
        "Have ... been running",
        "Having ... been running",
        "Has ... been running",
        "Had ... been running"
      ],
      [
        "have ... been waiting",
        "has ... been waiting",
        "had ... been waited",
        "have ... waited"
      ],
    ]),
    Tuple2(
        /* + */ [
          "We _____ the meal before he came.",
          "Linda _____ before she fell asleep.",
          "I _____ all day long so I came home very tired.",
          "When it was 5 pm, Mike _____ for his girlfriend for 2 hours.",
          "On Sunday, they _____ the house for 3 hours, when the parents came.",
          /* - */ "She _____ the book for half an hour when he came.",
          "It _____ for an hour when the sun rose.",
          "You _____ on the phone for 15 minutes before he came in.",
          "I _____ the TV show for an hour when my friend called me.",
          "Nick wasn't tired. He _____ the whole day yesterday.",
          /* ? */ "___ the kids _____ football till the evening yesterday?",
          "___ it _____ all that evening?",
          "___ the population _____ before the war?",
          "How long ___ you _____ on your project before the brother came?",
          "Where ___ you _____ for him for 2 hours before he called you?",
        ], [
      /* + */ [
        "had been eating",
        "have been eating",
        "has been eating",
        "had being eating"
      ],
      [
        "had been crying",
        "has been crying",
        "have been crying",
        "had be crying"
      ],
      [
        "had been working",
        "have been working",
        "has been working",
        "had being working"
      ],
      [
        "had been waiting",
        "have been waiting",
        "has been waiting",
        "was been waiting"
      ],
      [
        "had been cleaning",
        "have been cleaning",
        "has been cleaning",
        "had been cleaned"
      ],
      /* - */ [
        "hadn't been reading",
        "haven't been reading",
        "hasn't been reading",
        "wasn't been reading"
      ],
      [
        "hadn't been raining",
        "haven't been raining",
        "hasn't been raining",
        "hadn't being raining"
      ],
      [
        "hadn't been talking",
        "haven't been talking",
        "hasn't been talking",
        "wasn't been talking"
      ],
      [
        "hadn't been watching",
        "haven't been watching",
        "hasn't been watching",
        "didn't been watching"
      ],
      [
        "hadn't been working",
        "haven't been working",
        "hasn't been working",
        "didn't been working"
      ],
      /* ? */ [
        "Had ... been playing",
        "Have ... been playing",
        "Has ... been playing",
        "Was ... been playing"
      ],
      [
        "Had ... been snowing",
        "Have ... been snowing",
        "Has ... been snowing",
        "Having ... been snowing"
      ],
      [
        "Had ... been growing",
        "Has ... been growing",
        "Have ... been growing",
        "Had ... been grown"
      ],
      [
        "had ... been working",
        "has ... been working",
        "have ... been working",
        "had ... been worked"
      ],
      [
        "had ... been waiting",
        "have ... been waiting",
        "has ... been waiting",
        "had ... waiting"
      ],
    ]),
    Tuple2(
        /* + */ [
          "By the end of November, she _____ long enough to get the first salary.",
          "By the end of week we _____ our project for a month.",
          "By 2024 he _____ in this house for 20 years.",
          "In 4 month he _____ this picture for 10 years.",
          "In a week the workers _____ this mansion for 2 months.",
          /* - */ "We _____ in the USA for 1 year by tne end of November.",
          "When you will come there in May, I _____ in Canada for 6 months.",
          "Sarah _____ in university for three years by September.",
          "By Christmas the boy _____ to swim for 2 months.",
          "In an hour we _____ the movies for 12 hours already.",
          /* ? */ "___ they _____ checkers for 3 hours by the end of the hour?",
          "___ you _____ TV for a long time by 10 pm?",
          "___ this computer _____ for 10.000 hours non-stop by the next day?",
          "___ we _____ here for 5 years already in a month?",
          "___ I _____ to music for 4 hours in some minutes?",
        ], [
      /* + */ [
        "will have been working",
        "will had been working",
        "will has been working",
        "will have being working"
      ],
      [
        "will have been creating",
        "will have been created",
        "would have been creating",
        "will has been creating"
      ],
      [
        "will have been living",
        "will has been living",
        "will had been living",
        "will has living"
      ],
      [
        "will have been painting",
        "will have been paint",
        "will has been painting",
        "will have being painting"
      ],
      [
        "will have been building",
        "will have been built",
        "will have been builded",
        "will had been building"
      ],
      /* - */ [
        "won't have been living",
        "won't have living",
        "won't has been living",
        "won't had been living"
      ],
      [
        "won't have been staying",
        "won't had been staying",
        "won't having been stayed",
        "won't having been staying"
      ],
      [
        "won't have been studying",
        "won't have being studying",
        "won't had been studying",
        "won't has been studying"
      ],
      [
        "won't have been learning",
        "won't had been learning",
        "won't having been learning",
        "don't have been learning"
      ],
      [
        "won't have been watching",
        "won't have been watched",
        "won't had been watching",
        "won't has been watching"
      ],
      /* ? */ [
        "Will ... have been playing",
        "Would ... have been playing",
        "Would ... has been playing",
        "Will ... has been playing"
      ],
      [
        "Will ... have been watching",
        "Would ... have been watching",
        "Will ... have been watched",
        "Will ... have watching"
      ],
      [
        "Will ... have been working",
        "Will ... has been working",
        "Will ... had been working",
        "Will ... have working"
      ],
      [
        "Will ... have been living",
        "Will ... has been living",
        "Will ... have been lived",
        "Will ... have lived"
      ],
      [
        "Will ... have been listening",
        "Will ... had been listening",
        "Will ... have been listened",
        "Will ... have being listening"
      ],
    ]),
    Tuple2(
        /* + */ [
          "The office _____ every day.",
          "The cats _____ every morning by Mrs. Smith.",
          "The book _____ by me yesterday.",
          "The pupils _____ to the museum a week ago.",
          "The dinner _____ soon.",
          /* - */ "The plants _____ every week.",
          "I _____ with my work.",
          "She _____ to the party by her friends.",
          "The letters _____ in time by him.",
          "The house _____ next month.",
          /* ? */ "___ he always ___ to the parties?",
          "___ they often ___ on the lessons?",
          "___ Sam ___ the present for his birthday last month?",
          "___ we ___ by them?",
          "___ I _____ the new car?",
        ], [
      /* + */ ["is cleaned", "am cleaned", "do cleaned", "does cleaned"],
      ["are fed", "are feed", "is feed", "do fed"],
      ["was read", "was readed", "were read", "is readed"],
      ["were taken", "were taked", "was taken", "does taken"],
      ["will be cooked", "will be cook", "will is cooked", "will cooked"],
      /* - */ [
        "aren't watered",
        "aren't water",
        "isn't watered",
        "doesn't watered"
      ],
      [
        "am not satisfied",
        "is not satisfied",
        "are not satisfied",
        "do not satisfied"
      ],
      ["wasn't invited", "didn't invited", "don't invited", "wasn't invite"],
      ["weren't sent", "weren't sended", "weren't send", "wasn't sent"],
      ["won't be built", "won't be builded", "won't be build", "won't built"],
      /* ? */ [
        "Is ... invited",
        "Are ... invited",
        "Does ... invited",
        "Is ... invite"
      ],
      ["Are ... asked", "Is ... asked", "Do ... ask", "Does ... ask"],
      ["Was ... given", "Was ... gave", "Was ... give", "Were ... given"],
      ["Were ... helped", "Was ... helped", "Were ... help", "Were ... helpt"],
      [
        "Will ... be shown",
        "Will ... shown",
        "Will ... showed",
        "Will ... be show"
      ],
    ]),
    Tuple2(
        /* + */ [
          "The car _____ now.",
          "The exams _____ by the students at the moment.",
          "The cat _____ by its owner yesterday at 7 pm.",
          "These films _____ the whole winter last year.",
          "The walls _____ from 10 am till 3 pm yesterday.",
          /* - */ "We _____ an interesting story at the moment.",
          "The book _____ right now.",
          "The children _____ English now.",
          "The cake _____ the whole morning yesterday.",
          "The windows _____ from 7 am till 9 am.",
          /* ? */ "Where ___ the car _____ now?",
          "Why ___ these books _____?",
          "___ the game _____ the whole evening yesterday?",
          "___ the cookies _____ from 2 pm till 4 pm that day?",
          "___ the new iPhone _____ now?",
        ], [
      /* + */ [
        "is being repaired",
        "are being repaired",
        "was being repaired",
        "does being repaired"
      ],
      [
        "are being written",
        "is being written",
        "were being written",
        "do being written"
      ],
      [
        "was being washed",
        "was been washed",
        "were been washed",
        "was being washing"
      ],
      [
        "were being shown",
        "was being showed",
        "was being shown",
        "did being shown"
      ],
      [
        "were being painted",
        "do being painted",
        "were been painted",
        "were be painted"
      ],
      /* - */ [
        "aren't being told",
        "aren't being tell",
        "aren't being telled",
        "isn't being told"
      ],
      [
        "isn't being read",
        "isn't being readed",
        "aren't being read",
        "isn't been read"
      ],
      [
        "aren't being taught",
        "didn't being taught",
        "hasn't being taught",
        "hadn't being taught"
      ],
      [
        "wasn't being made",
        "weren't being made",
        "didn't being made",
        "hasn't being made"
      ],
      [
        "weren't being cleaned",
        "wasn't being cleaned",
        "weren't being clean",
        "weren't being cleaning"
      ],
      /* ? */ [
        "is ... being painted",
        "is ... been painted",
        "are ... being painted",
        "is ... being painting"
      ],
      [
        "are ... being burnt",
        "is ... being burned",
        "was ... being burnt",
        "did ... being burnt"
      ],
      [
        "Was ... being played",
        "Was ... been played",
        "Was ... be played",
        "Were ... being played"
      ],
      [
        "Were ... being baked",
        "Did ... being baked",
        "Does ... being baked",
        "Was ... being baked"
      ],
      [
        "Is ... being presented",
        "Are ... being presented",
        "Does ... being presented",
        "Is ... been presented"
      ],
    ]),
    Tuple2(
        /* + */ [
          "The roof ___ just _____ .",
          "We ___ already _____ to their place.",
          "The lesson _____ by 3 o'clock yesterday.",
          "They _____ by their friends by 4 pm yesterday.",
          "The letter _____ by the end of the day.",
          /* - */ "The food for the party _____ yet.",
          "They _____ about the event yet.",
          "Our work _____ by 3 o'clock yesterday.",
          "The wall _____ by the time parents returned.",
          "He _____ by his friends by the end of the week.",
          /* ? */ "___ the dogs _____ yet?",
          "___ this old bridge _____ recently?",
          "How ___ the box _____ by 3 o'clock yesterday?",
          "___ the car _____ by the time the owner came?",
          "___ the house _____ by the evening tomorrow?",
        ], [
      /* + */ [
        "has ... been repaired",
        "have ... been repaired",
        "had ... been repaired",
        "was ... been repaired"
      ],
      [
        "have ... been invited",
        "were ... been invited",
        "was ... been invited",
        "did ... been invited"
      ],
      [
        "had been finished",
        "have been finished",
        "has been finished",
        "was been finished"
      ],
      [
        "had been visited",
        "has been visited",
        "have been visited",
        "had be visited"
      ],
      [
        "will have been sent",
        "will has been send",
        "would been sent",
        "will have been send"
      ],
      /* - */ [
        "hasn't been bought",
        "haven't been buyed",
        "haven't been bought",
        "hadn't been bought"
      ],
      ["haven't been told", "hasn't been told", "hadn't told", "haven't told"],
      [
        "hadn't been done",
        "haven't been done",
        "hasn't been done",
        "hadn't been did"
      ],
      [
        "hadn't been painted",
        "hadn't being painted",
        "hadn't being painting",
        "haven't been painted"
      ],
      [
        "won't have been visited",
        "won't has been visited",
        "won't been visited",
        "wouldn't have been visited"
      ],
      /* ? */ [
        "Have ... been fed",
        "Has ... been fed",
        "Had ... been fed",
        "Have ... been feed"
      ],
      [
        "Has ... been repaired",
        "Had ... been repaired",
        "Has ... being repaired",
        "Has ... been repair"
      ],
      [
        "had ... been delivered",
        "have ... been delivered",
        "has ... be delivered",
        "had ... be delivered"
      ],
      [
        "Had ... been fixed",
        "Have ... been fixed",
        "Had ... been fix",
        "Had ... being fixed"
      ],
      [
        "Will ... have been cleaned",
        "Will ... has been cleaned",
        "Would ... have been cleaned",
        "Would ... has been cleaned"
      ],
    ]),
    Tuple2(
        /* + */ [
          "He said that he _____ us the following week.",
          "She thought that we _____ her work.",
          "The boy hoped that his friends _____ football at that time the following day.",
          "The brother told me that he _____ me the game by that evening.",
          "John said that by the end of that week he _____ there for 2 months.",
          /* - */ "I told them that I _____ with their task.",
          "He thought that we _____ the train.",
          "Doris knew that she _____ in time.",
          "She said that she _____ in the garden the whole following day.",
          "My mate said that he _____ his project by the following day.",
          /* ? */ "Did she ask you if you _____ her at the station?",
          "Did Sarah ask what they _____ as a present for her?",
          "Did they ask you whether you _____ home at that time the following day?",
          "___ you _____ your task by the following week?",
          "___ the pool _____ by the end of the following month?",
        ], [
      /* + */ ["would meet", "will meet", "would met", "would be meet"],
      ["would do", "would does", "will do", "would did"],
      ["would be playing", "would be play", "would playing", "will be playing"],
      [
        "would have brought",
        "would have bring",
        "would brought",
        "will have brought"
      ],
      [
        "would have been working",
        "would has been working",
        "will have been working",
        "will has been working"
      ],
      /* - */ [
        "wouldn't help",
        "won't help",
        "will not help",
        "wouldn't helping"
      ],
      ["wouldn't catch", "wouldn't caught", "wouldn't catched", "won't catch"],
      ["wouldn't come", "wouldn't came", "won't come", "won't be come"],
      [
        "wouldn't be working",
        "wouldn't working",
        "wouldn't be work",
        "will not be working"
      ],
      [
        "wouldn't have finished",
        "wouldn't have finish",
        "won't have finished",
        "wouldn't has finished"
      ],
      /* ? */ ["would meet", "will meet", "would be meet", "would have met"],
      ["would buy", "would bought", "would buyed", "will buy"],
      ["would be driving", "will be driving", "would driving", "will driving"],
      [
        "Would ... have done",
        "Will ... have done",
        "Would ... has done",
        "Would ... had done"
      ],
      [
        "Would ... have opened",
        "Would ... have be opened",
        "Will ... have opened",
        "Would ... has opened"
      ],
    ]),
    Tuple2(
        /* + */ [
          "She said that she ___ cats.",
          "He told us that he _____ a film then.",
          "Mary hoped that her boyfriend ___ already ___ home.",
          "We were told that they _____ the day before yesterday.",
          "I said that I ___ as a shop assistant.",
          /* - */ "I was told that he _____ from Moscow the day before.",
          "He knew that she _____ .",
          "I heard that you ___ ill then.",
          "Mike knew that Ann _____ at the factory.",
          "He hoped that they _____ the solution.",
          /* ? */ "Did he ask if his mother ___ there then?",
          "Were they sure whether he _____ the exam the previous week?",
          "Did Alex ask if she _____ then?",
          "Did he ask whether he _____ for 5 or 6 hours?",
          "Did Jason ask you if you _____ her for many years?",
        ], [
      /* + */ ["liked", "likes", "like", "is liking"],
      ["was watching", "were watching", "watched", "is watching"],
      ["had ... arrived", "has ... arrived", "have ... arrived", "arrived"],
      ["had come", "has come", "have came", "have come"],
      ["worked", "has worked", "am working", "work"],
      /* - */ [
        "hadn't returned",
        "didn't returned",
        "didn't return",
        "hasn't returned"
      ],
      [
        "wasn't sleeping",
        "weren't sleeping",
        "isn't sleeping",
        "don't sleeping"
      ],
      ["weren't", "wasn't", "didn't", "didn't be"],
      ["didn't work", "doesn't work", "isn't work", "doesn't works"],
      ["had found", "did found", "had find", "has found"],
      /* ? */ ["was", "were", "is", "be"],
      ["had passed", "has passed", "have passed", "passed"],
      ["was dancing", "were dancing", "is dancing", "am dancing"],
      [
        "had been working",
        "had working",
        "has been working",
        "was been working"
      ],
      ["had known", "known", "knew", "has known"],
    ]),
  ];
}
