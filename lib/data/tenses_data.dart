import 'dart:ui';

import 'package:tuple/tuple.dart';

abstract class TensesData {
  static const List<String> groupNames = [
    'Основы',
    'Simple',
    'Continuous',
    'Perfect',
    'Perfect Continuous',
    'Пассивный залог',
    'Дополнительно'
  ];
  static const List<Color> groupColors = [
    const Color(0xFF535353),
    const Color(0xFF48CD5E),
    const Color(0xFFF8CF0C),
    const Color(0xFFD98736),
    const Color(0xFFD95A40),
    const Color(0xFF428DD9),
    const Color(0xFFA742D9)
  ];

  static const Map<int, Tuple2<List<String>, Color>> firstExercisesGroup = {
    0: Tuple2(
        ["Глагол 'to be'", "Present Simple", "Past Simple", "Future Simple"],
        Color(0xFF48CD5E))
  };

  static const Map<int, Tuple2<List<String>, Color>> tenses = {
    0: Tuple2(["Что такое времена?", "Глагол 'to be'"], Color(0xFF535353)),
    1: Tuple2(
        ["Present Simple", "Past Simple", "Future Simple"], Color(0xFF48CD5E)),
    2: Tuple2(["Present Continuous", "Past Continuous", "Future Continuous"],
        Color(0xFFF8CF0C)),
    3: Tuple2(["Present Perfect", "Past Perfect", "Future Perfect"],
        Color(0xFFD98736)),
    4: Tuple2([
      "Present Perfect Continuous",
      "Past Perfect Continuous",
      "Future Perfect Continuous"
    ], Color(0xFFD95A40)),
    5: Tuple2(["Simple", "Continuous", "Perfect"], Color(0xFF428DD9)),
    6: Tuple2(["Future-in-the-Past", "Согласование времён"], Color(0xFFA742D9)),
  };
  static List<String> tensesAsArray() {
    List<String> output = [];
    tenses.keys.forEach((key) {
      tenses[key].item1.forEach((value) {
        output.add(value);
      });
    });
    return output;
  }

  static List<Color> colorsAsArray() {
    List<Color> output = [];
    tenses.keys.forEach((key) {
      tenses[key].item1.forEach((value) {
        output.add(tenses[key].item2);
      });
    });
    return output;
  }
}
