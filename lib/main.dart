import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/screens/onboarding/onboarding_model.dart';
import 'package:tense/screens/onboarding/onboarding_screen.dart';
import 'package:tense/screens/tab_bar_screen.dart';
import 'utils/custom_scroll.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  _getPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final launchCounter = prefs.getInt('launches');
    if (launchCounter == null) {
      prefs.setInt('launches', 1);
    } else {
      prefs.setInt('launches', launchCounter + 1);
    }
  }

  _getPref();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.white,
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: CustomScroll(),
          child: child,
        );
      },
      theme: ThemeData(
          fontFamily: 'AvenirNext',
          bottomSheetTheme: BottomSheetThemeData(
            backgroundColor: Colors.transparent,
          )),
      home: TabBarScreen(),
    );
  }
}
