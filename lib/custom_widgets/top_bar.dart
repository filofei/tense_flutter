import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tense/data/tenses_data.dart';

class TopBar extends StatelessWidget {
  final String headerText;
  final Function handler;
  final bool hasTopSafeAreaMargin;

  TopBar({this.handler, this.headerText, this.hasTopSafeAreaMargin = true});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: hasTopSafeAreaMargin,
      child: Container(
        height: 70,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          children: <Widget>[
            Text(
              headerText,
              style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'AvenirNext',
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[800]),
            ),
            CupertinoButton(
                padding: EdgeInsets.zero,
                child: Icon(
                  Icons.close,
                  size: 40,
                  color: TensesData.groupColors[5],
                ),
                onPressed: handler),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
    );
  }
}
