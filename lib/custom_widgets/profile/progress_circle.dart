import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:tense/data/tenses_data.dart';

class ProgressCircle extends StatelessWidget {
  final double progress;
  final String name;

  const ProgressCircle(this.progress, this.name);

  @override
  Widget build(BuildContext context) {
    final progressText = '${(progress * 100).round()}%';
    return Column(
      children: <Widget>[
        CircularPercentIndicator(
          radius: 100,
          lineWidth: 10,
          backgroundColor: Color(0xFFC3CED9),
          progressColor: TensesData.groupColors[5],
          percent: progress,
          circularStrokeCap: CircularStrokeCap.round,
          center: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 2),
              Text(
                progressText,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontFamily: "AvenirNext",
                  color: TensesData.groupColors[5],
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 7),
        Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontFamily: "AvenirNext",
            color: TensesData.groupColors[5],
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
