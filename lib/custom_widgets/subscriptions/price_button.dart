import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tense/data/tenses_data.dart';

class PriceButton extends StatelessWidget {
  final Function handler;
  final String text;

  const PriceButton({Key key, this.handler, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 2,
      height: 40,
      child: CupertinoButton(
        padding: EdgeInsets.symmetric(horizontal: 20),
        color: TensesData.groupColors[2],
        borderRadius: BorderRadius.circular(20),
        child: AutoSizeText(
          text,
          maxLines: 1,
          minFontSize: 16,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontFamily: 'AvenirNext',
            fontWeight: FontWeight.w600,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        onPressed: handler,
      ),
    );
  }
}
