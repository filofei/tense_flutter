import 'package:flutter/material.dart';
import 'package:tense/data/tenses_data.dart';

class PriceCalcText extends StatelessWidget {
  final String text;
  const PriceCalcText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontFamily: 'AvenirNext',
            fontWeight: FontWeight.w600,
            fontSize: 16,
            color: TensesData.groupColors[5],
          ),
        ),
      ),
    );
  }
}
