import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tense/data/tenses_data.dart';

class OnboardingPage extends StatelessWidget {
  final String title;
  final String description;
  final SvgPicture image;

  const OnboardingPage({Key key, this.title, this.description, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.vertical,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'AvenirNext',
            fontWeight: FontWeight.w700,
            fontSize: 24,
            color: TensesData.groupColors[5],
          ),
        ),
        image,
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 40,
          ),
          child: Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w500,
              fontSize: 18,
              color: Colors.grey[600],
            ),
          ),
        ),
      ],
    );
  }
}
