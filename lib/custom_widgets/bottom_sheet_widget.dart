import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:tense/data/tenses_data.dart';

class BottomSheetWidget extends StatelessWidget {
  final String message;

  const BottomSheetWidget({
    Key key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.error,
              size: 45,
              color: TensesData.groupColors[5],
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: AutoSizeText(
                message,
                minFontSize: 16.0,
                maxLines: 5,
                style: TextStyle(
                  color: TensesData.groupColors[5],
                  fontWeight: FontWeight.w500,
                  fontFamily: 'AvenirNext',
                  fontSize: 18.0,
                ),
              ),
            ),
          ]),
    );
  }
}
