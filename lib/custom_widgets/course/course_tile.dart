import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tense/custom_widgets/course/course_tense_tile.dart';
import 'package:tense/screens/course/course_tile_model.dart';
import 'package:tense/screens/course_page/course_page_model.dart';
import 'package:tense/screens/course_page/course_page_screen.dart';

class CourseTile extends StatelessWidget {
  final CourseTileModel model;
  const CourseTile({Key key, this.model}) : super(key: key);

  List<Widget> _provideWidgetsList(BuildContext context) {
    List<Widget> output = [
      Expanded(
        flex: 2,
        child: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              this.model.title,
              minFontSize: 16,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: this.model.color,
                fontSize: 22,
              ),
            ),
          ),
        ),
      ),
      Container(height: 0.5, color: Colors.grey[350]),
    ];
    model.tenseTitles.asMap().forEach((index, value) {
      output.add(CourseTenseTile(
          index: index,
          title: model.tenseTitles[index],
          color: model.color,
          handler: () {
            final detailRoute = CoursePageScreen(
              model: CoursePageModel(tenseName: model.tenseTitles[index]),
            );
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => detailRoute));
          }));
    });
    return output;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: (MediaQuery.of(context).size.width * 0.09)),
      child: Column(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 2 / (2 * model.tenseTitles.length / 4),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[350],
                      offset: Offset(0, 3),
                      blurRadius: 8,
                    ),
                  ]),
              child: Column(
                children: _provideWidgetsList(context),
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
              ),
            ),
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }
}
