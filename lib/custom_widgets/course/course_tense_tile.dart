import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CourseTenseTile extends StatelessWidget {
  final int index;
  final String title;
  final Color color;
  final Function handler;

  const CourseTenseTile({
    Key key,
    this.index,
    this.title,
    this.color,
    this.handler,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: CupertinoButton(
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              width: 50,
              child: Center(
                  child: Container(
                width: 12,
                height: 12,
                decoration: BoxDecoration(
                    color: color,
                    borderRadius: BorderRadius.all(Radius.circular(6))),
              )),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: AutoSizeText(
                  title,
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  minFontSize: 14,
                  style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'AvenirNext',
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[700],
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 13, 19, 13),
              width: 9,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(4.5),
              ),
            ),
          ],
        ),
        onPressed: handler,
      ),
    );
  }
}
