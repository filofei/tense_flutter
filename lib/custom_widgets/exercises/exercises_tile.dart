import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tense/screens/exercise_selection/exercise_selection_screen.dart';
import 'package:tense/screens/exercise_selection/exercises_selection_model.dart';
import 'package:tense/screens/exercises/exercises_tile_model.dart';

class ExercisesTile extends StatelessWidget {
  final ExercisesTileModel model;

  const ExercisesTile({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              width: 50,
              child: Center(
                  child: Container(
                width: 12,
                height: 12,
                decoration: BoxDecoration(
                    color: model.color,
                    borderRadius: BorderRadius.all(Radius.circular(6))),
              )),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: AutoSizeText(
                  model.title,
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  minFontSize: 14,
                  style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'AvenirNext',
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[700],
                  ),
                ),
              ),
            )
          ],
        ),
        onPressed: () {
          final detailRoute = ExerciseSelectionScreen(
            model: ExercisesSelectionModel(tenseName: model.title),
          );
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => detailRoute));
        },
      ),
    );
  }
}
