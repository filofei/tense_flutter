import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExercisesHeader extends StatelessWidget {
  final String title;
  final Color color;

  const ExercisesHeader({Key key, this.title, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: TextStyle(
              fontSize: 16,
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
