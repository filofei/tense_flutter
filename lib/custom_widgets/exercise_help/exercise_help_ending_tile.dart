import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tense/screens/exercise_help/ending_tile_model.dart';

class ExerciseHelpEndingTile extends StatelessWidget {
  final EndingTileModel model;

  const ExerciseHelpEndingTile({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12),
      height: 85,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              model.ending,
              style: TextStyle(
                fontSize: 18,
                fontFamily: 'AvenirNext',
                color: model.color,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: AutoSizeText(
                      model.rule,
                      maxLines: 4,
                      minFontSize: 12,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'AvenirNext',
                        color: Colors.grey[600],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: AutoSizeText(
                      model.sample,
                      textAlign: TextAlign.right,
                      minFontSize: 12,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'AvenirNext',
                        color: Colors.grey[900],
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
