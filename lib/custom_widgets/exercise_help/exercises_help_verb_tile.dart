import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tense/data/tenses_data.dart';
import 'package:tense/screens/exercise_help/verb_tile_model.dart';

class ExercisesHelpVerbTile extends StatelessWidget {
  final VerbTileModel model;

  const ExercisesHelpVerbTile({Key key, this.model}) : super(key: key);

  Widget _provideRowItem(String text, Color color) {
    return Align(
      alignment: Alignment.center,
      child: AutoSizeText(
        text,
        minFontSize: 13,
        style: TextStyle(
          fontSize: 16,
          fontFamily: 'AvenirNext',
          color: color,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12),
      height: 85,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                model.rus,
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'AvenirNext',
                  color: Colors.grey[600],
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _provideRowItem(model.v1, TensesData.groupColors[5]),
                _provideRowItem(model.v2, Colors.grey[900]),
                _provideRowItem(model.v3, Colors.grey[900]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
