import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/data/tenses_data.dart';

class SwitchCell extends StatefulWidget {
  final bool state;
  final String title;
  final String prefsKey;
  SwitchCell({Key key, this.title, this.state, this.prefsKey})
      : super(key: key);

  @override
  _SwitchCellState createState() => _SwitchCellState();
}

class _SwitchCellState extends State<SwitchCell> {
  bool state;

  _setValueToPrefs(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, state);
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      if (state == null) {
        state = widget.state;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.title,
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.grey[800],
            ),
          ),
          Switch.adaptive(
              value: state,
              activeColor: TensesData.groupColors[5],
              onChanged: (value) {
                setState(() {
                  state = value;
                  _setValueToPrefs(widget.prefsKey);
                });
              })
        ],
      ),
    );
  }
}
