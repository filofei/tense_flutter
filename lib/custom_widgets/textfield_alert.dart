import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/utils/shared_prefs_keys.dart';

class TextfieldAlert extends StatefulWidget {
  const TextfieldAlert({
    Key key,
    @required this.context,
    @required this.title,
    @required this.callback,
  }) : super(key: key);

  final BuildContext context;
  final String title;
  final Function callback;

  @override
  _TextfieldAlertState createState() => _TextfieldAlertState();
}

class _TextfieldAlertState extends State<TextfieldAlert> {
  String _text;

  _setName(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPrefsKeys.userName, name == null ? '' : name);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        widget.title,
        style: TextStyle(
          fontFamily: 'AvenirNext',
          fontWeight: FontWeight.w600,
          fontSize: 20,
        ),
      ),
      content: TextField(
        onChanged: (text) {
          setState(() {
            _text = text;
          });
        },
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Закрыть',
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            _setName(_text);
            Navigator.of(context).pop();
            widget.callback();
          },
          child: Text(
            'Ок',
            style: TextStyle(
              fontFamily: 'AvenirNext',
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ),
      ],
      elevation: 12,
    );
  }
}
