import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class TabBarTopBar extends StatelessWidget {
  final String headerText;
  final Function handler;

  TabBarTopBar({this.handler, this.headerText});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 70,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          children: <Widget>[
            Text(
              headerText,
              style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'AvenirNext',
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[800]),
            ),
            Container(
              height: 32,
              width: 74,
              child: CupertinoButton(
                  color: Color(0xFFFFE436),
                  padding: EdgeInsets.all(0),
                  child: SizedBox.expand(
                    child: Center(
                      child: Text(
                        'Tense+',
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'AvenirNext',
                            fontWeight: FontWeight.w600,
                            color: Colors.black87),
                      ),
                    ),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(16.0)),
                  onPressed: handler),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
    );
  }
}
