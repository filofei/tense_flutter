import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tense/data/tenses_data.dart';

class ExerciseSelectionTile extends StatelessWidget {
  final String assetName;
  final bool checked;
  final String title;
  final Function handler;

  const ExerciseSelectionTile(
      {Key key, this.assetName, this.checked, this.title, this.handler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey[350],
              offset: Offset(0, 3),
              blurRadius: 10,
            ),
          ]),
      child: SizedBox.expand(
        child: CupertinoButton(
          borderRadius: BorderRadius.circular(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SvgPicture.asset(
                assetName,
                height: 70,
                width: 70,
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: AutoSizeText(
                  title,
                  maxLines: 2,
                  minFontSize: 15,
                  style: TextStyle(
                    fontFamily: 'AvenirNext',
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black87,
                  ),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              (checked)
                  ? Icon(
                      Icons.check_circle_outline,
                      size: 50,
                      color: TensesData.groupColors[1],
                    )
                  : SizedBox(
                      width: 0,
                    )
            ],
          ),
          onPressed: handler,
        ),
      ),
    );
  }
}
