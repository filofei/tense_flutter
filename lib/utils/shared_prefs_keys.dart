abstract class SharedPrefsKeys {
  static const sound = 'sound';
  static const push = 'push';
  static const userName = 'userName';
  static const imagePath = 'imagePath';
  static const onboardingDidShow = 'onboardingDidShow';
}
