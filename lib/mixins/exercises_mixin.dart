import 'package:flutter/material.dart';
import 'package:tense/screens/exercises_result/exercises_result_model.dart';
import 'package:tense/screens/exercises_result/exercises_result_screen.dart';

mixin ExercisesMixin {
  openResultScreen(
      int mistakesCount, int type, String tenseName, BuildContext context) {
    final testRoute = ExercisesResultScreen(
      model: ExercisesResultModel(mistakesCount, type, tenseName),
    );
    Navigator.push(context, MaterialPageRoute(builder: (context) => testRoute));
  }
}
