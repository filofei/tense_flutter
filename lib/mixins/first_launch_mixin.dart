import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tense/custom_widgets/bottom_sheet_widget.dart';

mixin FirstLaunchMixin {
  Future alreadyLaunched(String screenName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final launched = prefs.getBool(screenName);
    return launched;
  }

  setFirstLaunch(String screenName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(screenName) == null) {
      prefs.setBool(screenName, true);
    }
  }

  showMessageIfFirstLaunch(
      Widget screenRef, String message, BuildContext context) {
    final screenName = screenRef.runtimeType.toString();
    alreadyLaunched(screenName).then((value) {
      if (value == null) {
        setFirstLaunch(screenName);
        Timer(Duration(milliseconds: 300), () {
          showModalBottomSheet(
            context: context,
            builder: (context) => BottomSheetWidget(
              message: message,
            ),
          );
        });
      }
    });
  }
}
